package plug.core.fx;

import java.util.function.Consumer;
import javafx.scene.layout.Region;
import plug.utils.file.FileWatcher;

public interface IAppContext {

	FileWatcher getFileWatcher();

	void openView(String title, Region view, Consumer<Region> onClose);

	default void openView(String title, Region view) {
		openView(title, view, null);
	}

}

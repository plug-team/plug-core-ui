package plug.ui2;

import javafx.application.Application;
import javafx.scene.Node;
import org.kohsuke.args4j.*;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;

public class PlugUI extends AbstractUIBase {

    public PlugUI() throws IOException { }

    @Argument(metaVar = "<model-path>",usage = "The path to the model file")
    Path modelPath;

    @Option(name = "-p", aliases = {"--property-file"}, usage = "The path to the .gpsl property file")
    Path propertiesPath;

    @Option(name="-h", aliases = {"--help"}, usage="Print this help message and exists", help = true)
    boolean help;

    public void printHelp(PrintStream printStream, CmdLineParser parser) {
        String className = this.getClass().getCanonicalName();
        printStream.println("java " + className + " [options...] arguments...");
        // print the list of available options
        parser.printUsage(printStream);
        printStream.println();

        // print option sample. This is useful some time
        printStream.println("  Example: java " + className +" "+parser.printExample(OptionHandlerFilter.REQUIRED) + " [<model-path>]");
        System.exit(1);
    }

    @Override
    public Node getMainComponent() {
        CmdLineParser parser = new CmdLineParser(this);
        try {
            parser.parseArgument(this.getParameters().getRaw());
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            printHelp(System.err, parser);
            System.exit(1);
        }

        if (help) {
            printHelp(System.out, parser);
            System.exit(0);
        }
        try {
            return new VerificationMainView(modelPath, propertiesPath, this);
        } catch (Throwable e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        return null;
    }

    public static void main(String[] args) {

        Application.launch(args);
    }
}

package plug.ui2.simulation.expression_watcher;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.VBox;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import plug.core.IAtomicPropositionsEvaluator;
import plug.core.IConfiguration;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.events.ExecutionEndedEvent;
import plug.events.OpenConfigurationEvent;
import plug.explorer.BFSExplorer;
import plug.folding.runtime.PreinitializedRuntime;
import plug.statespace.SimpleStateSpaceManager;
import plug.utils.graph.algorithms.DijkstraShortestPath;
import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.interpreter.Evaluator;
import properties.PropositionalLogic.interpreter.atom.AtomArrayValuationEvaluator;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;


public class ExpressionsWatcher<C, T> extends VBox {
    ObservableList<WatchExpression> theWatchExpList = FXCollections.observableArrayList(WatchExpression.NO_EXPRESSION);
    Evaluator watchEvaluator;
    AtomArrayValuationEvaluator apEvaluator;
    IAtomicPropositionsEvaluator atomicPropositionsEvaluator;
    List<String> atomList = new ArrayList<>();
    ObjectProperty<C> inConfiguration;
    ObjectProperty<ITransitionRelation<C, T>> inTransitionRelationProperty;
    Consumer<List<C>> inCounterExampleHandler;
    ListView<WatchExpression> listView;

    public ExpressionsWatcher(
            ObjectProperty<C> inConfiguration,
            ObjectProperty<ITransitionRelation<C, T>> transitionRelationProperty,
            Consumer<List<C>> counterExampleHandler) {
        this.inConfiguration = inConfiguration;
        this.inTransitionRelationProperty = transitionRelationProperty;
        this.inCounterExampleHandler = counterExampleHandler;
        listView = new ListView<>(theWatchExpList);

        listView.setCellFactory(CheckBoxListCell.forListView(WatchExpression::selectedProperty));
        listView.setCellFactory(WatchExpressionListCell.forListView(WatchExpression::selectedProperty, new WatchExpressionConverter(theWatchExpList)));
        listView.setEditable(true);
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.getSelectionModel().selectFirst();

        ContextMenu contextMenu = new ContextMenu();

        MenuItem goUntilTrueMenuItem = new MenuItem("Go until true");
        goUntilTrueMenuItem.setOnAction(this::goUntilTrue);
        MenuItem goUntilFalseMenuItem = new MenuItem("Go until false");
        goUntilFalseMenuItem.setOnAction(this::goUntilFalse);

        contextMenu.getItems().addAll(goUntilTrueMenuItem, goUntilFalseMenuItem);

        setOnContextMenuRequested((cme) -> {
            List<WatchExpression> weList = listView.getSelectionModel()
                    .getSelectedItems()
                    .stream()
                    .filter(e -> e != WatchExpression.NO_EXPRESSION)
                    .collect(Collectors.toList());
            if (weList.size() < 1) return;
            //we need to get the window, so that the ContextMenu disappears on left click
            //@url(https://bugs.openjdk.java.net/browse/JDK-8095591)
            contextMenu.show(((Node)cme.getTarget()).getScene().getWindow(), cme.getScreenX(), cme.getScreenY());
        });

        listView.setOnEditStart(t ->
            listView.setPrefHeight((theWatchExpList.size()-1)*25 + 60));
        listView.setOnEditCommit(t -> {
            if (t.getNewValue() == WatchExpression.NO_EXPRESSION) return;
            if(listView.getSelectionModel().getSelectedIndices().contains(listView.getItems().size()-1)) {
                listView.getItems().add(WatchExpression.NO_EXPRESSION);
            }

            listView.getItems().set(t.getIndex(), t.getNewValue());
        });

        //listView.setOnEditCancel(t -> System.out.println("setOnEditCancel"));
        listView.setPrefHeight(theWatchExpList.size()*25);

        this.getChildren().add(listView);
        this.setFillWidth(true);

        theWatchExpList.addListener(this::listChangedCallback);

        setAtomicPropositionsEvaluator();

        transitionRelationProperty.addListener((v, o, n) -> setAtomicPropositionsEvaluator());

        inConfiguration.addListener(this::configurationChangedCallback);
    }

    void setAtomicPropositionsEvaluator() {
        if (inTransitionRelationProperty.get() == null) return;
        atomicPropositionsEvaluator = inTransitionRelationProperty.get().getAtomicPropositionEvaluator();
        if (atomicPropositionsEvaluator == null || atomList == null || atomList.isEmpty()) return;
        try {
            atomicPropositionsEvaluator.registerAtomicPropositions(atomList.toArray(new String[0]));
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
            return;
        }
    }

    PropositionalLogicModelFactory factory = PropositionalLogicModelFactory.eINSTANCE;

    void goUntilTrue(ActionEvent ae) {
        goUntil(true);
    }
    void goUntilFalse(ActionEvent ae) {
        goUntil(false);
    }

    LogicalNegation negation(Expression exp) {
        LogicalNegation neg = factory.createLogicalNegation();
        neg.setOperand(exp);
        return  neg;
    }
    void goUntil(boolean goTrue) {
        List<Expression> weList = listView.getSelectionModel()
                .getSelectedItems()
                .stream()
                .filter(e -> e != WatchExpression.NO_EXPRESSION)
                .map(e -> EcoreUtil.copy(e.expression))
                .collect(Collectors.toList());

        if (weList.size() < 1) return;

        Expression breakpointCondition = goTrue ? weList.get(0) : negation(weList.get(0));
        if (weList.size() > 1) {

            int idx = 1;
            for (; idx < weList.size(); idx++) {
                LogicalConjunction conjunction = factory.createLogicalConjunction();
                conjunction.setLhs(breakpointCondition);
                conjunction.setRhs(goTrue ? weList.get(idx) : negation(weList.get(idx)));
                breakpointCondition = conjunction;
            }
        }

        //prepare the evaluator
        Map<String, Integer> theAtomMap = new HashMap<>();
        List<String> theAtomList = new ArrayList<>();
        retrieveEcoreAtoms(weList, theAtomList, theAtomMap);
        Evaluator evaluator = new Evaluator();
        evaluator.setDefaultEvaluator(apEvaluator = new AtomArrayValuationEvaluator(theAtomMap));

        if (atomicPropositionsEvaluator == null) return;
        try {
            atomicPropositionsEvaluator.registerAtomicPropositions(theAtomList.toArray(new String[0]));
        } catch (Exception ex) {
            System.err.println(ex.getLocalizedMessage());
            return;
        }

        final Expression conditionToCheck = breakpointCondition;
        BooleanProperty conditionWasChecked = new SimpleBooleanProperty(false);
        PreinitializedRuntime<C, T> preinitializedRuntime = new PreinitializedRuntime<>(inTransitionRelationProperty.get(), Collections.singleton(inConfiguration.get()));
        IStateSpaceManager stateSpaceManager = new SimpleStateSpaceManager<>();
        stateSpaceManager.parentTransitionStorage();
        IExecutionController bfsExplorer = new BFSExplorer(preinitializedRuntime, stateSpaceManager);

        bfsExplorer.getAnnouncer().when(OpenConfigurationEvent.class, (ann, e) -> {
            apEvaluator.setValuation(atomicPropositionsEvaluator.getAtomicPropositionValuations((IConfiguration) e.getConfiguration()));
            boolean evaluationResult = evaluator.evaluate(conditionToCheck);
            if ( evaluationResult ) {
                e.getSource().getMonitor().hasToFinish();
                conditionWasChecked.set(true);

                List<C> counterExample = new DijkstraShortestPath()
                        .getShortestPath(
                                stateSpaceManager.getGraphView(),
                                stateSpaceManager.initialConfigurations(),
                                e.getConfiguration());
                inCounterExampleHandler.accept(counterExample);
            }

        });
        bfsExplorer.getAnnouncer().when(ExecutionEndedEvent.class, (ann, e) ->  {
            if (!conditionWasChecked.get()) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "The breakpoint cannot be reached from the current configuration");
                alert.setTitle("Note");
                alert.setHeaderText(null);
                alert.showAndWait();
            }
        });
        bfsExplorer.execute();
    }

    void retrieveAllAtoms() {
        //retrieve all atoms
        atomList.clear();
        Map<String, Integer> atomMap = new HashMap<>();
        retrieveAtoms(listView.getItems(), atomList, atomMap);

        watchEvaluator = new Evaluator();
        watchEvaluator.setDefaultEvaluator(apEvaluator = new AtomArrayValuationEvaluator(atomMap));
        if (atomicPropositionsEvaluator == null || atomList.isEmpty()) return;
        try {
            atomicPropositionsEvaluator.registerAtomicPropositions(atomList.toArray(new String[0]));
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
            return;
        }
    }

    void updateValues(C inConfiguration) {
        apEvaluator.setValuation(atomicPropositionsEvaluator.getAtomicPropositionValuations((IConfiguration) inConfiguration));
        for (WatchExpression exp : theWatchExpList) {
            exp.updateValuation(watchEvaluator);
        }
    }

    void listChangedCallback(ListChangeListener.Change c) {
        retrieveAllAtoms();
        if (inConfiguration != null && inConfiguration.get() != null) {
            updateValues(inConfiguration.get());
        }
        listView.setPrefHeight(theWatchExpList.size()*25);
    }

    void configurationChangedCallback(ObservableValue<? extends C> observableValue, C old, C newC) {
        if (newC == null || apEvaluator == null || atomicPropositionsEvaluator == null) return;
        retrieveAllAtoms();
        updateValues(inConfiguration.get());
    }

    void retrieveAtoms(List<WatchExpression> inWatchList, List<String> outAtomList, Map<String, Integer> outAtomMap) {

        retrieveEcoreAtoms(
                inWatchList
                        .stream()
                        .filter(e -> e != WatchExpression.NO_EXPRESSION)
                        .map(e -> e.expression).collect(Collectors.toList()),
                outAtomList,
                outAtomMap);

    }

    void retrieveEcoreAtoms(List<Expression> inExpressionList, List<String> outAtomList, Map<String, Integer> outAtomMap) {
        int idx = 0;

        for (Expression exp : inExpressionList) {
            if (exp == null) continue;
            if (exp instanceof Atom) {
                String code = ((Atom) exp).getCode();
                if (outAtomMap.put(code, idx++) == null) {
                    outAtomList.add(code);
                }
            }
            Iterator<EObject> iterator = EcoreUtil.getAllContents(exp, true);
            while (iterator.hasNext()) {
                EObject current = iterator.next();
                if (current instanceof Atom) {
                    String code = ((Atom) current).getCode();
                    if (outAtomMap.put(code, idx++) == null) {
                        outAtomList.add(code);
                    }
                }
            }
        }
    }
}


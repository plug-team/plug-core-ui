package plug.ui2.simulation.configuration;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.IRuntimeView;
import plug.utils.ui.fx.GuiFxUtils;

import java.util.function.Consumer;

public class ConfigurationTreeViewTitledPane<C, T> extends TitledPane {
    FontIcon expandConfigurationIcon =  new FontIcon("gmi-expand-more:18:#2ecc71");
    FontIcon diffConfigurationIcon =  new FontIcon("gmi-developer-mode:18:#2ecc71");
    HBox configurationToolbar = new HBox(expandConfigurationIcon, diffConfigurationIcon);
    ConfigurationTreeView<C, T> configurationTreeView;

    public ConfigurationTreeViewTitledPane(ObjectProperty<C> inCurrentConfiguration,
                                           ObjectProperty<IRuntimeView<C, T>> inRuntimeViewProperty)
    {
        configurationTreeView = new ConfigurationTreeView<>(
                inCurrentConfiguration,
                inRuntimeViewProperty,
                false,
                false);

        setText("Configuration");
        setContent(configurationTreeView);

        setGraphic(configurationToolbar);
        setExpanded(true);

        expandConfigurationIcon.setOnMouseClicked(me -> {
            configurationTreeView.setExpandedTree(!configurationTreeView.expandedTree.get());
            expandConfigurationIcon.setRotate((expandConfigurationIcon.getRotate() + 180) % 360);
        });
        StringBinding expandBinding = Bindings.createStringBinding(() -> configurationTreeView.expandedTree.get() ? "Collapse tree" : "Expand tree", configurationTreeView.expandedTree);
        GuiFxUtils.addTooltip(expandBinding, expandConfigurationIcon);
        GuiFxUtils.addButtonEffects(expandConfigurationIcon);

        diffConfigurationIcon.setOnMouseClicked(me -> configurationTreeView.setShowDifferencesOnly(!configurationTreeView.showDifferencesOnly.get()));
        StringBinding diffBinding = Bindings.createStringBinding(()->configurationTreeView.showDifferencesOnly.get() ? "Show all items" : "Show only differences", configurationTreeView.showDifferencesOnly);
        GuiFxUtils.addTooltip(diffBinding, diffConfigurationIcon);
        GuiFxUtils.addButtonEffects(diffConfigurationIcon);

        //filter the mouse released event so that the titlearea does not collapse when clicking the button
        addEventFilter(MouseEvent.MOUSE_RELEASED, e -> {
            EventTarget target = e.getTarget();
            if (target.equals(expandConfigurationIcon) || target.equals(diffConfigurationIcon)) {
                e.consume();
            }
        });
    }

    public void addToolbarAction(String iconDescription, StringBinding text, Consumer<Event> value) {
        FontIcon icon = new FontIcon(iconDescription);
        //add the action to the TitledPane toolbar
        configurationToolbar.getChildren().add(icon);
        icon.setOnMouseClicked(value::accept);
        GuiFxUtils.addTooltip(text, icon);
        GuiFxUtils.addButtonEffects(icon);
        this.addEventFilter(MouseEvent.MOUSE_RELEASED, e -> {
            EventTarget target = e.getTarget();
            if (target.equals(icon)) {
                e.consume();
            }
        });


        //add the action to the contextual menu
        if (configurationTreeView != null) {
            configurationTreeView.addContextMenuAction(iconDescription, text, value);
        }
    }
}

package plug.cli;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;
import plug.core.ILanguagePlugin;
import plug.core.ITransitionRelation;
import plug.core.RuntimeDescription;
import plug.core.execution.Execution;
import plug.core.registry.LanguageModuleRegistry;
import plug.core.ui.common.GPSL2Execution;
import plug.explorer.BFSExplorer;
import plug.explorer.DFSExplorer;
import plug.language.buchikripke.runtime.KripkeBuchiProductSemantics;
import plug.simulation.trace_storage.TraceEntry;
import plug.statespace.transitions.FiredTransition;
import plug.utils.Pair;
import plug.utils.marshaling.Marshaller;
import plug.verifiers.deadlock.DeadlockVerifier;
import plug.visualisation.StateSpace2DOT;
import plug.visualisation.StateSpace2ExplicitVHD;
import plug.visualisation.StateSpace2MTX;
import plug.visualisation.StateSpace2TGF;
import properties.LTL.transformations.LTL2Buchi;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.Collections;

class VerificationMain {
    @Argument(metaVar = "<model-path>", usage = "The path to the model file")
    Path modelPath;

    @Option(name = "-p", aliases = {"--property-file"}, usage = "The path to the .gpsl property file")
    Path propertiesPath;

    @Option(name = "-v", aliases = {"--verify"}, metaVar = "<property-name>", usage = "The name of the property to verify", depends = {"-p"})
    String propertyToVerify;

    enum ExplorerEnum {
        BFS, DFS
    }

    @Option(name = "-x", aliases = {"--explore"}, usage = "State-space exploration", forbids = {"-v", "-d"})
    ExplorerEnum explorer = ExplorerEnum.BFS;

    @Option(name = "-d", aliases = {"--deadlock"}, usage = "Check for deadlocks", forbids = {"-v"})
    boolean checkForDeadlocks;

    @Option(name = "-h", aliases = {"--help"}, usage = "Print this help message and exists", help = true)
    boolean help;

    @Option(name = "-b", aliases = {"--heartbeat"}, usage = "Interactive binary heartbeat", forbids = {"-progress"})
    boolean heartbeat = false;

    @Option(name = "-progress", usage = "Show progress during execution")
    boolean showProgress = false;

    enum ExportFormatEnum {
        TGF,
        MTX,
        DOT,
        VHD
    }

    @Option(name = "-e", aliases = {"--export"}, usage = "Export the state-space")
    ExportFormatEnum exportFormat;

    @Option(name = "-o", aliases = {"--output"}, usage = "Export output file", depends = {"-e"})
    Path outputPath;

    @Option(name = "-ts", aliases = "--transition-storage", usage = "Set the transition storage backend")
    Execution.TransitionStorageType transitionStorageType = Execution.TransitionStorageType.PARENT;

    public ILanguagePlugin<ITransitionRelation> getLanguagePlugin() {
        ILanguagePlugin<ITransitionRelation> languagePlugin = LanguageModuleRegistry.getInstance().getModuleByExtension(modelPath);
        if (languagePlugin == null) {
            System.err.println("Could not load the languagePlugin for " + modelPath);
            return null;
        }
        return languagePlugin;
    }

    public RuntimeDescription getRuntimeDescription() {
        return new RuntimeDescription(modelPath);
    }

    public void getHelp() {
        PlugCLI.printHelp(System.out);
    }

    @SuppressWarnings("unchecked")
    void run() {
        if (help) {
            getHelp();
            return;
        }

        //load the language plugin
        ILanguagePlugin<ITransitionRelation> languagePlugin = getLanguagePlugin();
        if (languagePlugin == null) {
            return;
        }

        Execution execution = null;
        if (propertyToVerify != null) {
            //LTL model checking
            System.out.println("LTL model-checking");

            try {
                RuntimeDescription modelDescription = getRuntimeDescription();
                execution = new GPSL2Execution().execution(propertyToVerify, propertiesPath, modelDescription, transitionStorageType);
            } catch (Exception e) {
                if (e instanceof LTL2Buchi.PropertyNotFoundException) {
                    System.err.println(e.getMessage());
                    return;
                }
                e.printStackTrace();
                return;
            }
        } else if (checkForDeadlocks) {
            //Deadlock checking
            System.out.println("Deadlock checking");
            execution = new Execution(
                    "Deadlock",
                    modelPath,
                    languagePlugin,
                    DeadlockVerifier::bfs,
                    transitionStorageType,
                    Collections.emptySet());
        } else {
            //if not LTL or Deadlock, then just exploration
            switch (explorer) {
                case BFS:
                    //create BFS explorer
                    System.out.println("BFS state-space exploration");
                    execution = new Execution(
                            "BFS",
                            modelPath,
                            languagePlugin,
                            BFSExplorer::new,
                            transitionStorageType,
                            Collections.emptySet());
                    break;
                case DFS:
                    //create DFS explorer
                    System.out.println("DFS state-space exploration");
                    execution = new Execution(
                            "DFS",
                            modelPath,
                            languagePlugin,
                            DFSExplorer::new,
                            transitionStorageType,
                            Collections.emptySet());
                    break;
            }
        }

        if (execution == null) {
            System.err.println("Could not create the execution instance");
            return;
        }

        execution.initialize();
        Thread runnerThread = execution.run();

        boolean stopped = false;
        if (heartbeat) {
            //stopped = handleRequests(execution);

            final Execution exe = execution;
            boolean stop[] = new boolean[] {false};
            Thread heartbeat = new Thread(()->stop[0] = handleRequests(exe));
            heartbeat.run();
            try {
                heartbeat.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stopped = stop[0];
        } else {
            if (showProgress) {
                while (runnerThread.isAlive()) {
                    try {
                        long confs = execution.configurationCount();
                        System.out.print(confs + " configurations [" + (confs / Math.max(execution.getElapsedTime() / 1000, 1)) + " confs/sec]\r");
                        Thread.sleep(500);
                    } catch (InterruptedException e) {

                    }
                }
            }
        }

        try {
            runnerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (!heartbeat) {
            System.out.println(
                    "Model loaded in " + execution.getLoadingTime() + " ms\n"
                            + "\"" + execution.getName() + "\" Analysis " + (stopped ? Execution.CompletenessStatus.INCOMPLETE : execution.resultStatus()) + " " + execution.configurationCount() + " states "
                            + execution.transitionCount() + " transitions"
                            + " in " + execution.getElapsedTime() + " ms\n" +
                            (propertyToVerify != null || checkForDeadlocks ? execution.verificationStatus() : "")

            );
        }

        handleStateSpaceExport(execution);
    }

    void handleStateSpaceExport(Execution execution) {
        if (exportFormat != null) {
            switch (exportFormat) {
                case TGF:
                    System.out.println("Exporting the state-space to TGF");
                    StateSpace2TGF.toTGF(execution.getGraphView(), outputPath.toAbsolutePath().toString());
                    System.out.println("TGF state-space generated " + outputPath.toAbsolutePath().toString());
                    break;
                case MTX:
                    System.out.println("Exporting the state-space to MTX");
                    StateSpace2MTX.toMTX(execution.getGraphView(), outputPath.toAbsolutePath().toString());
                    System.out.println("MTX state-space generated " + outputPath.toAbsolutePath().toString());
                    break;
                case DOT:
                    System.out.println("Exporting the state-space to DOT");
                    StateSpace2DOT.toDOT(execution.getGraphView(), false, outputPath.toAbsolutePath().toString());
                    System.out.println("DOT state-space generated " + outputPath.toAbsolutePath().toString());
                    break;
                case VHD:
                    System.out.println("Exporting the state-space to VHDL");
                    StateSpace2ExplicitVHD.toVHD(execution.getGraphView(), modelPath.toString(), outputPath.toAbsolutePath().toString());
                    System.out.println("VHDL state-space generated " + outputPath.toAbsolutePath().toString());
                    break;
                default: //nothing
            }
        }
    }

    void handleBinaryCounterExample(Execution execution) throws InterruptedException {
        if (execution.verificationStatus() == Execution.VerificationStatus.UNKNOWN) {
            try {
                //no counter example, since unknown status, thus 0 transitions
                //serialize the number of transitions
                Marshaller.writeInt(0, System.out);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        if (!execution.traceStore.hasCounterExample()) {
            try {
                //serialize the number of transitions
                Marshaller.writeInt(0, System.out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            theCounterExampleLength = 0;
            execution.traceStore.setAddStepUserCallback(this::counterExampleLength);
            execution.traceStore.iterateOverTrace();
            try {
                //serialize the number of transitions
                Marshaller.writeInt(theCounterExampleLength, System.out);
                //System.err.println("len : " + theCounterExampleLength);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int counterLength = theCounterExampleLength;
            execution.traceStore.setAddStepUserCallback(this::addStepCallback);
            execution.traceStore.iterateOverTrace();
            if (theCounterExampleLength != 0) {
                throw new RuntimeException("The counter example length "+ counterLength +" does not match the real length " + theCounterExampleLength);
            }
        }
        System.out.print('\n');
    }

    int theCounterExampleLength = 0;
    void counterExampleLength(Object in) {
        TraceEntry entry = (TraceEntry) in;
        if (    propertyToVerify != null
                && entry.getConfiguration().getClass().getCanonicalName().equals("plug.language.buchikripke.runtime.KripkeBuchiConfiguration")
                && entry.getAction().getClass().getCanonicalName().equals("plug.utils.Pair")
                && entry.getAction() != null) {
            //if KripkeBuchiConfiguration && the Action is a Pair -> the thePair.a is the remote runtime transition
            Pair thePair = (Pair) entry.getAction();
            FiredTransition theFired = (FiredTransition)thePair.a;
            if (theFired.getAction().getClass().getCanonicalName().equals("plug.language.remote.runtime.FireableTransition")) {
                theCounterExampleLength++;
            }
            else if (theFired.getAction() == KripkeBuchiProductSemantics.StutteringTransition.instance) {
                //do not count the StutteringTransition to the counter example
            }
        }
        if (checkForDeadlocks) {
            if (entry.getAction().getClass().getCanonicalName().equals("plug.language.remote.runtime.FireableTransition")) {
                theCounterExampleLength++;
            }
        }
    }
    void addStepCallback(Object in) {
        TraceEntry entry = (TraceEntry) in;
        //check for kripke/buchi trace
        if (    propertyToVerify != null
                && entry.getConfiguration().getClass().getCanonicalName().equals("plug.language.buchikripke.runtime.KripkeBuchiConfiguration")
                && entry.getAction().getClass().getCanonicalName().equals("plug.utils.Pair")
                && entry.getAction() != null) {
            //if KripkeBuchiConfiguration && the Action is a Pair -> the thePair.a is the remote runtime transition
            Pair thePair = (Pair) entry.getAction();
            FiredTransition theFired = (FiredTransition)thePair.a;
            if (theFired.getAction().getClass().getCanonicalName().equals("plug.language.remote.runtime.FireableTransition")) {
                Object theAction = theFired.getAction();
                printRemoteByteArrayAction(theAction);
                theCounterExampleLength--;
            }
        }

        if (checkForDeadlocks) {
            if (entry.getAction().getClass().getCanonicalName().equals("plug.language.remote.runtime.FireableTransition")) {
                Object theAction = entry.getAction();
                printRemoteByteArrayAction(theAction);
                theCounterExampleLength--;
            }
        }
    }

    void printRemoteByteArrayAction(Object theAction) {
        try {
            Field dataField = theAction.getClass().getField("data");
            if (dataField.getType() == byte[].class) {
                byte[] data = (byte[]) dataField.get(theAction);
                //serialize the transition length
                Marshaller.writeInt(data.length, System.out);
                //serialize the transition itself
                Marshaller.write(data, System.out);

                //System.err.println(data.length + " " + Arrays.toString(data));
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean handleRequests(Execution execution) {
        //Execution execution = executions[0];
        BufferedInputStream bis = new BufferedInputStream(System.in);

        try {
            while (true) {
                switch (bis.read()) {
                    case 'h': //*h*eartbeat
                        long loading = execution.getLoadingTime();
                        long elapsed = execution.getElapsedTime();
                        long configurations = execution.configurationCount();
                        Execution.CompletenessStatus completenessStatus = execution.resultStatus();
                        Execution.VerificationStatus verificationStatus = execution.verificationStatus();

                        System.out.println("heartbeat [loading: " + loading
                                + ", elapsed: " + elapsed
                                + ", configurations: " + configurations
                                + ", completeness: " + completenessStatus
                                + ", verification: " + verificationStatus + "]");
                        break;
                    case 'p': //*p*ause
                        execution.pause();
                        System.out.println("Paused");
                        break;
                    case 's': //*s*top
                        boolean stopped = true;
                        if (execution.status() == Execution.Status.FINISHED) { stopped = false; }
                        execution.stop();
                        System.out.println("Stopped");
                        return stopped;
                    case 'r': //*r*esume
                        execution.resume();
                        System.out.println("Resumed");
                        break;
                    case 'w': //*w*itness
                        handleBinaryCounterExample(execution);
                        break;
                    default: //nothing
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
}

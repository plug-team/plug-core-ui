package plug.ui2;


import javafx.scene.control.TextArea;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class OutputRedirect extends TextArea {
    OutputStream stdErr = System.err;
    OutputStream stdOut = System.out;

    PrintStream errStream = new PrintStream(new OutputStream() {
        @Override
        public void write(int b) throws IOException {
            stdErr.write(b);
            appendText(String.valueOf((char)b));
        }
    });
    PrintStream outStream = new PrintStream(new OutputStream() {
        @Override
        public void write(int b) throws IOException {
            stdOut.write(b);
            appendText(String.valueOf((char)b));
        }
    });

    public OutputRedirect() {
        this.setEditable(false);
        System.setErr(errStream);
        System.setOut(outStream);
    }
}

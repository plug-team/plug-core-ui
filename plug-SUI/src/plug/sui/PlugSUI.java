package plug.sui;

import org.kohsuke.args4j.*;
import plug.utils.marshaling.Unmarshaller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.invoke.MethodHandles;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;

public class PlugSUI {

    static CmdLineParser parser;

    public static void main(String args[]) {
        ServerMain main = new ServerMain();
        parser = new CmdLineParser(main);
        try {
            parser.parseArgument(args);
            main.run();
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            printHelp(System.err);
            System.exit(1);
        }
    }

    public static void printHelp(PrintStream printStream) {
        String className = MethodHandles.lookup().lookupClass().getCanonicalName();
        printStream.println("java " + className + " [options...] arguments...");
        // print the list of available options
        parser.printUsage(printStream);
        printStream.println();

        // print option sample. This is useful some time
        printStream.println("  Example: java " + className +" "+parser.printExample(OptionHandlerFilter.REQUIRED) + " <model-path>");
        System.exit(1);
    }
}

class ServerMain {
    @Option(name = "-p", aliases = {"--port-number"}, usage = "The port number")
    int portNumber = 3456;

    @Option(name= "-h", aliases = {"--help"}, usage="Print this help message and exists", help = true)
    boolean help;

    void run() {
        if (help) {
            PlugSUI.printHelp(System.out);
            return;
        }

        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket socket;
            while (true) {
                socket = serverSocket.accept();
                System.out.println("Client connected on " + socket.toString() );
                new ServerAPI(socket).run();

            }

        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }
}

class ServerAPI implements Runnable  {
    Socket socket;
    private BufferedInputStream inputStream;
    private BufferedOutputStream outputStream;

    public ServerAPI(Socket socket) throws IOException {
        this.socket = socket;

        inputStream = new BufferedInputStream(socket.getInputStream());
        outputStream = new BufferedOutputStream(socket.getOutputStream());
    }
    @Override
    public void run() {
        try {
            //noinspection StatementWithEmptyBody
            while (handleRequest());

            socket.close();
        } catch (IOException e){
            e.printStackTrace(System.err);
        }
    }

    boolean handleRequest() throws IOException {
        byte id = Unmarshaller.readByte(inputStream);
        System.out.println(" id = " + id);
        if (id == 2) {
            byte requestID = Unmarshaller.readByte(inputStream);
            System.out.println(" requestID = " + requestID);
            api(requestID);
            return true;
        }
        return false;
    }

    void api(int requestID) {
        switch (requestID) {
            case 1:
                System.out.println("Request " + requestID + " received");

                break;
            default:
                System.err.println("Unknown request ID = " + requestID);
        }
    }
}

```
./obp2-headless
Argument "<model-path>" is required
java plug.cli.PlugCLI [options...] arguments...
 <model-path>                           : The path to the model file
 -b (--heartbeat)                       : Interactive binary heartbeat
                                          (default: false)
 -d (--deadlock)                        : Check for deadlocks (default: false)
 -e (--export) [TGF | MTX | DOT | VHD]  : Export the state-space
 -h (--help)                            : Print this help message and exists
                                          (default: false)
 -o (--output) PATH                     : Export output file
 -p (--property-file) PATH              : The path to the .gpsl property file
 -progress                              : Show progress during execution
                                          (default: false)
 -ts (--transition-storage) [NONE |     : Set the transition storage backend
 COUNTING | PARENT | FULL]                (default: PARENT)
 -v (--verify) <property-name>          : The name of the property to verify
 -x (--explore) [BFS | DFS]             : State-space exploration (default: BFS)

  Example: java plug.cli.PlugCLI  <model-path>
```

# Heartbeat commands are

- `h` : shows the status of the analysis on STDOUT
- `p` : pauses the analysis
- `r` : resumes the analysis
- `s` : stops the analysis
- `w` : print the witness trace (the counter example) in binary format

# The format of the binary counter example is

The number of transitions (4 bytes) -- 0 if no counter-example -- followed by transition count transitions.
Each transitions starts with its *size* (4 bytes), followed by size bytes of transition content.
A `\n` is appended after the counter example.

```
[4:transitions_count]
(
    [4:transition_size]
    [transition_size:transition]
){transitions_count}
\n
```


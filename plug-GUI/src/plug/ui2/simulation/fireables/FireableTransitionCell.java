package plug.ui2.simulation.fireables;

import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableSet;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.IRuntimeView;
import plug.utils.ui.fx.GuiFxUtils;

import java.util.function.Consumer;

class FireableTransitionCell<C, T> extends ListCell<T> {
    private final ObjectProperty<IRuntimeView<C, T>> runtimeViewProperty;
    private final ObservableSet<T> alreadyFiredSet;

    HBox hbox = new HBox();
    Label label = new Label("(empty)");
    FontIcon stepOverIcon =  new FontIcon("gmi-play-arrow:30:#2ecc71");

    // TODO add a step into button

    public FireableTransitionCell(Consumer<T> fireCallback,
                                  ObjectProperty<IRuntimeView<C, T>> runtimeViewProperty,
                                  ObservableSet<T> inAlreadyFiredSet) {
        super();
        this.runtimeViewProperty = runtimeViewProperty;
        this.alreadyFiredSet = inAlreadyFiredSet;

        Tooltip tooltip = new Tooltip("Fire transition");
        tooltip.setFont(Font.font("Times"));
        Tooltip.install(stepOverIcon, tooltip);

        GuiFxUtils.addButtonEffects(stepOverIcon);

        hbox.getChildren().addAll(
                stepOverIcon
                , label
        );
        hbox.setAlignment(Pos.CENTER_LEFT);
        stepOverIcon.setOnMouseClicked(e -> fireCallback.accept(getItem()));
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);  // No text in label of super class
        if (empty) {
            setGraphic(null);
        } else {
            String text = null;
            if (runtimeViewProperty.get() != null) {
                text = runtimeViewProperty.get().getFireableTransitionDescription(item);
            }

            label.setText(text != null ? text : item.toString());
            if (alreadyFiredSet.contains(item)) {
                label.setTextFill(Color.GRAY);
                stepOverIcon.setIconColor(Color.GRAY);
            } else {
                label.setTextFill(Color.BLACK);
                stepOverIcon.setIconColor(Color.valueOf("#2ecc71"));
            }

            setGraphic(hbox);
        }
    }
}
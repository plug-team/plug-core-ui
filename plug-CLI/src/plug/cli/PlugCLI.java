package plug.cli;

import org.kohsuke.args4j.*;
import plug.core.ILanguagePlugin;
import plug.core.ITransitionRelation;
import plug.core.RuntimeDescription;
import plug.core.execution.Execution;
import plug.core.registry.LanguageModuleRegistry;
import plug.core.ui.common.GPSL2Execution;
import plug.explorer.BFSExplorer;
import plug.explorer.DFSExplorer;
import plug.language.buchikripke.runtime.KripkeBuchiProductSemantics;
import plug.simulation.trace_storage.TraceEntry;
import plug.statespace.transitions.FiredTransition;
import plug.utils.Pair;
import plug.utils.marshaling.Marshaller;
import plug.verifiers.deadlock.DeadlockVerifier;
import plug.visualisation.StateSpace2DOT;
import plug.visualisation.StateSpace2ExplicitVHD;
import plug.visualisation.StateSpace2MTX;
import plug.visualisation.StateSpace2TGF;
import properties.LTL.transformations.LTL2Buchi;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.Collections;

public class PlugCLI {

    static CmdLineParser parser;

    public static void main(String[] args) {
        VerificationMain main = new VerificationMain();
        parser = new CmdLineParser(main);
        try {
            parser.parseArgument(args);
            main.run();
        } catch (Throwable e) {
            System.err.println(e.getMessage());
            printHelp(System.err);
            System.exit(1);
        }
    }

    public static void printHelp(PrintStream printStream) {
        String className = MethodHandles.lookup().lookupClass().getCanonicalName();
        printStream.println("java " + className + " [options...] arguments...");
        // print the list of available options
        parser.printUsage(printStream);
        printStream.println();

        // print option sample. This is useful some times
        printStream.println("  Example: java " + className + " " + parser.printExample(OptionHandlerFilter.REQUIRED) + " <model-path>");
        System.exit(1);
    }
}


package plug.ui2.model_view;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.function.Function;

import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import plug.code_editor.richtextfx.FiacreStyler;
import plug.code_editor.richtextfx.GPSLPropertyStyler;
import plug.code_editor.richtextfx.TLAStyler;

/**
 * Created by Ciprian TEODOROV on 05/03/17.
 */
public class CodeEditor extends StackPane {
    final KeyCombination saveKeyCombination = new KeyCodeCombination(KeyCode.S,
            KeyCombination.SHORTCUT_DOWN);

    private final CodeArea codeArea;
    ObjectProperty<Path> pathProperty;


    private Function<String, StyleSpans<Collection<String>>> styler;

    private Function<String, StyleSpans<Collection<String>>> defaultStyler() {
       FiacreStyler fcrStyler = new FiacreStyler();
        return fcrStyler::computeHighlighting;
    }

    public CodeEditor(ObjectProperty<Path> pathProperty, BooleanProperty modelChanged) {
        this(pathProperty, modelChanged, null);
    }

    public CodeEditor(ObjectProperty<Path> pathProperty, BooleanProperty modelChanged, Function<String, StyleSpans<Collection<String>>> styler) {
        this.pathProperty = pathProperty;
        this.styler = styler;
        this.codeArea = new CodeArea();
        codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));

//        codeArea.richChanges()
//                .filter(ch -> !ch.getInserted().equals(ch.getRemoved()))
//                .subscribe(change -> codeArea.setStyleSpans(0, );

        pathProperty.addListener((observable, oldValue, newValue) -> updateText(readFile(newValue)));
        modelChanged.addListener((observable, oldValue, newValue) -> updateText(readFile(pathProperty.get())));

        updateText(readFile(pathProperty.get()));

        VirtualizedScrollPane<CodeArea> scrollPane = new VirtualizedScrollPane<>(codeArea);
        this.getChildren().add(scrollPane);
        this.getStylesheets().add(CodeEditor.class.getResource("java-keywords.css").toExternalForm());

        setOnKeyPressed((event) -> {
            if (event.isMetaDown() && event.getText().equals("z")) {
                codeArea.undo();
            }
            if (event.isMetaDown() && event.isShiftDown() && event.getText().equals("z")) {
                codeArea.redo();
            }
        });

        codeArea.addEventHandler(KeyEvent.KEY_RELEASED, this::saveKeyCombinationHandler );
    }

    protected void updateText(String text) {
        codeArea.replaceText(text);
        codeArea.setStyleSpans(0, styler == null ? defaultStyler().apply(codeArea.getText()) : styler.apply(codeArea.getText()));
        codeArea.getUndoManager().forgetHistory();
        // Set caret to the beginning of the file to avoid to scroll all the file
        codeArea.moveTo(0);
        codeArea.requestFollowCaret();
    }

    void saveKeyCombinationHandler(KeyEvent e) {
        if (saveKeyCombination.match(e)) {
            String text = codeArea.getText();
            try {
                Path savePath = pathProperty.get();
                if (savePath == null) {
                    FileChooser chooser = new FileChooser();
                    File file = chooser.showSaveDialog(getScene().getWindow());
                    if (file == null) return;
                    savePath = file.toPath();
                }

                Files.write(savePath, text.getBytes(StandardCharsets.UTF_8));
                pathProperty.set(savePath);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    String readFile(Path path) {
        if (path == null) return "";
        try {
            if (path.getFileName().toString().endsWith(".tla")) {
                styler = new TLAStyler()::computeHighlighting;
            } else if (path.getFileName().toString().endsWith(".gpsl")) {
                styler = new GPSLPropertyStyler()::computeHighlighting;
            }
            else {
                styler = new FiacreStyler()::computeHighlighting;
            }

            return new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
        } catch (IOException e) {
            return "unsupported file type";
        }
    }
}

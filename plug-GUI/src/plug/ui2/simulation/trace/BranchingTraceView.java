package plug.ui2.simulation.trace;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.IFiredTransition;
import plug.core.IRuntimeView;
import plug.core.ITransitionRelation;
import plug.simulation.trace_storage.TraceEntry;
import plug.simulation.trace_storage.TraceStore;
import plug.utils.ui.graph.GraphView;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class BranchingTraceView<C, T> extends BorderPane {
    ObjectProperty<TraceStore<C, Object>> traceStoreProperty;
    GraphView<TraceEntry<C, Object>, Object> graphView;
    SimulationGraphViewModel<C, T> graphViewModel;

    ObjectProperty<ITransitionRelation<C, T>> transitionRelationProperty;
    ObjectProperty<IRuntimeView<C, T>> runtimeViewProperty;

    public BranchingTraceView(
            ObjectProperty<ITransitionRelation<C, T>> transitionRelationProperty,
            ObjectProperty<IRuntimeView<C, T>> runtimeViewProperty,
            ObjectProperty<IFiredTransition<C,?>> inFiredTransition,
            ObjectProperty<TraceStore<C, Object>> inoutTraceStore,
            ObjectProperty<TraceEntry<C, Object>> outCurrentEntry,
            Function<TraceEntry<C, T>, Node> configurationViewBuilder) {

        graphView = new GraphView<>();
        graphView.setPrefSize(400, 300);
        graphViewModel = new SimulationGraphViewModel<>(transitionRelationProperty, runtimeViewProperty, configurationViewBuilder);
        graphView.setModel(graphViewModel);

        this.traceStoreProperty = inoutTraceStore;
        TraceStore<C, Object> traceStore = traceStoreProperty.get();
        if (traceStore == null) {
            traceStoreProperty.set( traceStore = new TraceStore<>() );
        }
        traceStore.setAddRootUserCallback(this::addRootCallback);
        traceStore.setAddStepUserCallback(this::addStepCallback);
        traceStore.iterateOverTrace();
        graphView.setSelectedVertex(traceStore.getLastEntry());
        if (traceStore.getLastEntry() != null) {
            outCurrentEntry.set(traceStore.getLastEntry());
        }

        this.transitionRelationProperty = transitionRelationProperty;
        this.runtimeViewProperty = runtimeViewProperty;

        graphView.setNodeFolding(true);
        graphView.beHorizontal();

        setCenter(graphView);

        // Allows to hide details (diffs)
        ToggleButton hideDetailsButton = new ToggleButton(null, new FontIcon("gmi-photo-size-select-small"));
        hideDetailsButton.setTooltip(new Tooltip("Show/hide details in configurations"));
        hideDetailsButton.selectedProperty().bindBidirectional(graphView.hideAllDetailsProperty());

        // Allows to hide colors
        ToggleButton coloredButton = new ToggleButton(null, new FontIcon("gmi-color-lens"));
        coloredButton.setTooltip(new Tooltip("Show/hide colors of configurations"));
        coloredButton.selectedProperty().bindBidirectional(graphView.coloredNodesProperty());

        // Allows to change orientation
        ToggleButton orientationButton = new ToggleButton(null, new FontIcon("gmi-screen-rotation"));
        orientationButton.setTooltip(new Tooltip("Change trace orientation"));
        orientationButton.selectedProperty().bindBidirectional(graphView.isVerticalProperty());

        Button saveImageButton = new Button(null, new FontIcon("gmi-save"));
        saveImageButton.setTooltip(new Tooltip("Save an image of the current trace"));
        saveImageButton.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Select file where to save image");
            chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG image", "*.png"));
            File file = chooser.showSaveDialog(this.getScene().getWindow());
            if (file != null) {
                try {
                    graphView.savePngImage(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        HBox toolkitPane = new HBox(5,
                hideDetailsButton, coloredButton, orientationButton, new Separator(Orientation.VERTICAL),
                saveImageButton//, openWatcherWindowButton
        );
        toolkitPane.setPadding(new Insets(5));
        setBottom(toolkitPane);

        graphView.selectedVertexProperty().addListener((a, o, n) -> {
            if (n == null) {
                outCurrentEntry.set(null);
                return;
            }
            outCurrentEntry.set(n);
        });

        inFiredTransition.addListener((a, o, n) -> {
            TraceEntry<C, Object> sourceEntry = graphView.getSelectedVertex();
            TraceEntry<C, Object> firstBranch = null;
            for (C target : n.getTargets()) {
                TraceEntry<C, Object> targetEntry = traceStoreProperty.get().addStep(sourceEntry, target, n.getAction());
                if (firstBranch == null) {
                    firstBranch = targetEntry;
                }
            }
            graphView.setSelectedVertex(firstBranch);
        });
    }

    public void setInitialConfigurations(Set<C> initialConfigurations) {
        traceStoreProperty.get().addRoots(initialConfigurations);
    }

    public void setCounterExample(List<C> counterExample) {
        TraceEntry<C, Object> entry = traceStoreProperty.get().addSteps(
                graphView.getSelectedVertex(),
                counterExample,
                transitionRelationProperty.get()::actionFromSourceToTarget);
        graphView.setSelectedVertex(entry);
    }

    void addStepCallback(TraceEntry<C, Object> entry) {
        graphViewModel.addEdge(entry.getParent(), entry, entry.getAction());
    }

    BooleanProperty isFirst = new SimpleBooleanProperty(true);
    void addRootCallback(TraceEntry<C, Object> entry) {
        graphViewModel.getInitialVertices().add(entry);
        if (isFirst.get()) {
            graphView.setSelectedVertex(entry);
            isFirst.set(false);
        }
    }
}

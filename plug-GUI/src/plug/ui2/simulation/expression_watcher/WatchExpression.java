package plug.ui2.simulation.expression_watcher;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Alert;
import properties.PropositionalLogic.PropositionalLogicModel.Expression;
import properties.PropositionalLogic.interpreter.Evaluator;
import properties.PropositionalLogic.parser.Parser;

import java.util.Objects;

class WatchExpression {

    public final static WatchExpression NO_EXPRESSION = new WatchExpression(null);

    String expressionText;
    Expression expression;
    BooleanProperty selectedProperty = new SimpleBooleanProperty();
    BooleanProperty isTrue = new SimpleBooleanProperty(false);

    public BooleanProperty selectedProperty() {
        return selectedProperty;
    }

    WatchExpression(String expressionText) {
        this.expressionText = expressionText;
        if ( expressionText!= null ) {
            expression = Parser.parse(expressionText, (e -> {
                Alert alert =  new Alert(Alert.AlertType.ERROR, "The expression '"+expressionText+"' cannot be parsed.\n" + e.getLocalizedMessage());
                alert.setTitle("Proposition Compilation error");
                alert.setHeaderText(null);
                alert.showAndWait();
                //System.err.println("The expression '"+expressionText+"' cannot be parsed.\n" + e.getLocalizedMessage());
            }));
        }
    }
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof WatchExpression)) return false;
        return Objects.equals(expressionText, ((WatchExpression) other).expressionText);
    }

    @Override
    public String toString() {
        if (this == NO_EXPRESSION) return "NO_EXPRESSION_TAG";
        return (selectedProperty.get() ? "\u25CF " : "\u25CB ") + " " + expressionText;
    }

    public void updateValuation(Evaluator evaluator) {
        if (this == NO_EXPRESSION) return;
        if (expression == null) {
            return;
        }
        isTrue.set(evaluator.evaluate(expression));
    }
}

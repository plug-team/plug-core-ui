package plug.ui2.simulation.expression_watcher;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.fxmisc.richtext.CodeArea;
import org.kordamp.ikonli.javafx.FontIcon;

class WatchExpressionListCell extends ListCell<WatchExpression> {
    private CodeArea codeArea;

    private final CheckBox selectedCheckBox = new CheckBox();
    private final Label expressionLabel = new Label();

    private ObservableValue<Boolean> booleanProperty;

    private final FontIcon evaluationResultIcon =  new FontIcon("gmi-lens:20");
    private final GridPane mGraphic = new GridPane();

    public static Callback<ListView<WatchExpression>, ListCell<WatchExpression>> forListView(
            final Callback<WatchExpression, ObservableValue<Boolean>> getSelectedProperty,
            final StringConverter<WatchExpression> converter) {
        return list -> new WatchExpressionListCell(getSelectedProperty, converter);
    }

    public WatchExpressionListCell(
            final Callback<WatchExpression, ObservableValue<Boolean>> getSelectedProperty,
            final StringConverter<WatchExpression> converter) {
        this.getStyleClass().add("text-field-list-cell");
        setConverter(converter);
        setSelectedStateCallback(getSelectedProperty);

        setAlignment(Pos.CENTER_LEFT);
        setContentDisplay(ContentDisplay.LEFT);

        mGraphic.add(selectedCheckBox, 0, 0);
        mGraphic.add(expressionLabel, 1, 0);
        mGraphic.add(evaluationResultIcon, 2, 0);
        ColumnConstraints c1 = new ColumnConstraints();
        ColumnConstraints c2 = new ColumnConstraints();
        c2.setHgrow(Priority.ALWAYS);
        ColumnConstraints c3 = new ColumnConstraints();
        mGraphic.setHgap(5);

        mGraphic.getColumnConstraints().addAll(c1, c2, c3);

        // by default the graphic is null until the cell stops being empty
        setGraphic(null);


    }

    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/

    // --- converter
    private ObjectProperty<StringConverter<WatchExpression>> converter =
            new SimpleObjectProperty<>(this, "converter");

    /**
     * The {@link StringConverter} property.
     */
    public final ObjectProperty<StringConverter<WatchExpression>> converterProperty() {
        return converter;
    }

    /**
     * Sets the {@link StringConverter} to be used in this cell.
     */
    public final void setConverter(StringConverter<WatchExpression> value) {
        converterProperty().set(value);
    }

    /**
     * Returns the {@link StringConverter} used in this cell.
     */
    public final StringConverter<WatchExpression> getConverter() {
        return converterProperty().get();
    }

    /** {@inheritDoc} */
    @Override public void startEdit() {
        if (! isEditable() || ! getListView().isEditable()) {
            return;
        }
        super.startEdit();

        if (isEditing()) {
            if (codeArea == null) {
                codeArea = new CodeArea(getItemText(this, getConverter()));

                KeyCombination saveKeyCombination = new KeyCodeCombination(KeyCode.ENTER,
                        KeyCombination.SHORTCUT_DOWN);
                codeArea.setOnKeyReleased(t -> {
                    if (saveKeyCombination.match(t)) {
                        commitEdit(getConverter().fromString(codeArea.getText()));
                        t.consume();
                    }
                    if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                        t.consume();
                    }
                });
            }
            FontIcon okIkon = new FontIcon("gmi-check-circle:20:#2ecc71");
            okIkon.setOnMouseClicked(ae -> {
                commitEdit(getConverter().fromString(codeArea.getText()));
                ae.consume();
            });
            HBox hb = new HBox(codeArea, okIkon);
            HBox.setHgrow(codeArea, Priority.ALWAYS);
            setText(null);
            setGraphic(hb);
            codeArea.selectAll();
            codeArea.requestFocus();
            codeArea.setPrefHeight(60);
        }
    }

    @Override public void cancelEdit() {
        super.cancelEdit();
        updateItem(this.getItem(), false);
    }

    String getItemText(Cell<WatchExpression> cell, StringConverter<WatchExpression> converter) {
        return converter == null ?
                cell.getItem() == null ? "" : cell.getItem().toString() :
                converter.toString(cell.getItem());
    }

    // --- selected state callback property
    private ObjectProperty<Callback<WatchExpression, ObservableValue<Boolean>>>
            selectedStateCallback =
            new SimpleObjectProperty<Callback<WatchExpression, ObservableValue<Boolean>>>(
                    this, "selectedStateCallback");

    /**
     * Property representing the {@link Callback} that is bound to by the
     * CheckBox shown on screen.
     */
    public final ObjectProperty<Callback<WatchExpression, ObservableValue<Boolean>>> selectedStateCallbackProperty() {
        return selectedStateCallback;
    }

    /**
     * Sets the {@link Callback} that is bound to by the CheckBox shown on screen.
     */
    public final void setSelectedStateCallback(Callback<WatchExpression, ObservableValue<Boolean>> value) {
        selectedStateCallbackProperty().set(value);
    }

    /**
     * Returns the {@link Callback} that is bound to by the CheckBox shown on screen.
     */
    public final Callback<WatchExpression, ObservableValue<Boolean>> getSelectedStateCallback() {
        return selectedStateCallbackProperty().get();
    }

    /** {@inheritDoc} */
    @Override public void updateItem(WatchExpression item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setGraphic(null);
            setText(null);
            return;
        }

        StringConverter<WatchExpression> converter = getConverter();
        Callback<WatchExpression, ObservableValue<Boolean>> callback = getSelectedStateCallback();
        if (callback == null) {
            throw new NullPointerException(
                    "The CheckBoxListCell selectedStateCallbackProperty can not be null");
        }

        if (isEditing()) {
            if (codeArea != null) {
                codeArea.replaceText(getItemText(this, converter));
            }
            setText(null);
            setGraphic(codeArea);

            return;
        }
        if (item == WatchExpression.NO_EXPRESSION) {
            setText("Click to enter a new watch");
            setGraphic(null);
            return;
        }

        setText(null);
        setGraphic(mGraphic);
        String text = converter != null ? converter.toString(item) : (item == null ? "" : item.toString());
        expressionLabel.setText(text.length() < 28 ? text : text.substring(0, 28) + "...");

        if (booleanProperty != null) {
            selectedCheckBox.selectedProperty().unbindBidirectional((BooleanProperty)booleanProperty);
        }
        booleanProperty = callback.call(item);
        if (booleanProperty != null) {
            selectedCheckBox.selectedProperty().bindBidirectional((BooleanProperty)booleanProperty);
        }

        if (item != null && item.isTrue.get()) {
            evaluationResultIcon.setIconColor(Color.GREEN);
        } else {
            evaluationResultIcon.setIconColor(Color.RED);
        }
        item.isTrue.addListener((v, o, n) -> {
            if (item != null && item.isTrue.get()) {
                evaluationResultIcon.setIconColor(Color.GREEN);
            } else {
                evaluationResultIcon.setIconColor(Color.RED);
            }
        });
    }
}

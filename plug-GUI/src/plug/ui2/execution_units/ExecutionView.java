package plug.ui2.execution_units;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.util.Duration;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.execution.Execution;
import plug.core.fx.IAppContext;
import plug.simulation.trace_storage.TraceStore;
import plug.ui2.simulation.SimulationView;
import plug.utils.ui.fx.GuiFxUtils;

import java.util.Collections;
import java.util.Set;
import java.util.function.Consumer;

public class ExecutionView extends GridPane {
    Execution execution;

    protected final FontIcon controlIcon = new FontIcon("gmi-play-arrow:30");
    protected final FontIcon simulationIcon = new FontIcon("gmi-directions-run:20");
    protected final FontIcon stopIcon = new FontIcon("gmi-stop:20");
    protected final FontIcon clearIcon = new FontIcon("gmi-delete:15");
    protected final FontIcon progressIcon = new FontIcon("gmi-settings:15");
    protected final Label nameLabel = new Label();
    protected final Label detailsLabel = new Label();

    protected final Timeline blinkingAnimation = new Timeline(20,
            new KeyFrame(Duration.valueOf("1s"), "rotate", new KeyValue(progressIcon.rotateProperty(), 360)),
            new KeyFrame(Duration.ZERO, "update item", event -> updateView())
    );

    Consumer<Execution> onExecutionError;
    IAppContext appContext;

    public ExecutionView(
            Execution execution,
            Consumer<Execution> onExecutionError,
            IAppContext appContext) {
        this.execution = execution;
        this.onExecutionError = onExecutionError;
        this.appContext = appContext;

        blinkingAnimation.setCycleCount(Animation.INDEFINITE);
        detailsLabel.setFont(new Font(10));

        GuiFxUtils.addButtonEffects(controlIcon);
        GuiFxUtils.addButtonEffects(simulationIcon);
        GuiFxUtils.addButtonEffects(stopIcon);
        GuiFxUtils.addButtonEffects(clearIcon);

        add(nameLabel, 1, 0);
        add(detailsLabel, 1, 1);
        add(controlIcon, 0, 0, 1, 2);
        add(simulationIcon, 3, 0, 1, 2);

        getColumnConstraints().add(new ColumnConstraints(60));
        getColumnConstraints().add(new ColumnConstraints(40, 60, Double.MAX_VALUE, Priority.ALWAYS, HPos.CENTER, true));
        getColumnConstraints().add(new ColumnConstraints(10));
        getRowConstraints().add(new RowConstraints(20, 20, 20, Priority.NEVER, VPos.CENTER, false));
        getRowConstraints().add(new RowConstraints(20, 20, 20, Priority.NEVER, VPos.CENTER, false));

        Tooltip tooltip = new Tooltip("Click for trace simulation");
        tooltip.setFont(Font.font("Times"));

        Tooltip.install(simulationIcon, tooltip);
        updateView();
    }

    void updateView() {
        switch (execution.status) {
            case CREATED:
            case INITIALIZED:
                controlIcon.setIconLiteral("gmi-play-arrow:30:black"); //run
                controlIcon.setOnMouseClicked(c -> {
                    if (execution.status == Execution.Status.CREATED) {
                        execution.initialize();
                    }
                    execution.run();
                    updateView();
                });

                nameLabel.setGraphic(null);
                detailsLabel.setGraphic(null);
                blinkingAnimation.stop();
                break;
            case RUNNING:
                controlIcon.setIconLiteral("gmi-pause:30:black");      //pause
                controlIcon.setOnMouseClicked(c -> {
                    execution.pause();
                    updateView();
                });

                nameLabel.setGraphic(stopIcon);                        //stop
                stopIcon.setOnMouseClicked(c -> {
                    execution.stop();
                    updateView();
                });

                detailsLabel.setGraphic(progressIcon);
                blinkingAnimation.play();
                break;
            case PAUSED:
                controlIcon.setIconLiteral("gmi-play-arrow:30:black"); //resume
                controlIcon.setOnMouseClicked(c -> {
                    execution.resume();
                    updateView();
                });

                nameLabel.setGraphic(stopIcon);                        //stop
                stopIcon.setOnMouseClicked(c -> {
                    execution.stop();
                    updateView();
                });

                blinkingAnimation.stop();
                break;
            case STOPPED:
                controlIcon.setIconLiteral("gmi-play-arrow:30:black"); //resume
                controlIcon.setOnMouseClicked(c -> {
                    execution.resume();
                    updateView();
                });

                nameLabel.setGraphic(null);
                detailsLabel.setGraphic(null);
                blinkingAnimation.stop();
                break;
            case FINISHED:
                controlIcon.setIconLiteral("gmi-play-arrow:30:black"); //reset & run
                controlIcon.setOnMouseClicked(c -> {
                    execution.reset();
                    execution.run();
                    updateView();
                });

                nameLabel.setGraphic(clearIcon);
                clearIcon.setOnMouseClicked(c -> {
                    execution.reset();
                    updateView();
                });

                //if finished and violation the simulation icon is red
                if (execution.getViolation() != null) {
                    simulationIcon.setIconColor(Paint.valueOf("red"));
                } else {
                    simulationIcon.setIconColor(Paint.valueOf("green"));
                }

                detailsLabel.setGraphic(null);
                blinkingAnimation.stop();

                break;
            case FAILED:
                controlIcon.setIconLiteral("gmi-rotate-right:30:black"); //reset
                controlIcon.setOnMouseClicked(c -> {
                    execution.reset();
                    updateView();
                });

                nameLabel.setGraphic(null);
                detailsLabel.setGraphic(null);
                blinkingAnimation.stop();
                onExecutionError.accept(execution);
                break;
        }
        nameLabel.setText(execution.getName());
        detailsLabel.setText(execution.getDetails());
        simulationIcon.setOnMouseClicked(this::startSimulation);
    }

    @SuppressWarnings("unchecked")
    void startSimulation(MouseEvent event) {
        ObjectProperty<Set> initialConfigurationsProperty;
        ObjectProperty<TraceStore> executionTraceProperty = new SimpleObjectProperty<>(execution.traceStore);
        if (executionTraceProperty.get().getRoots().isEmpty()) {
            initialConfigurationsProperty = new SimpleObjectProperty<>(execution.getTransitionRelation().initialConfigurations());
        } else {
            initialConfigurationsProperty = new SimpleObjectProperty<>(Collections.emptySet());
        }
        SimulationView simulationView = new SimulationView(
                new SimpleObjectProperty<>(execution.getTransitionRelation()),
                new SimpleObjectProperty<>(execution.getRuntimeView()),
                initialConfigurationsProperty,
                executionTraceProperty);

        appContext.openView("Analyze execution '" + execution.getName() +"'", simulationView, null);
    }
}

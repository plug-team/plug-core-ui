package plug.ui2.simulation.configuration;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.IRuntimeView;
import plug.core.view.ConfigurationItem;

import java.util.*;
import java.util.function.Consumer;


/**
 * A {@link ConfigurationTreeView} presents an tree of {@link ConfigurationItem}s
 * computed from {@link plug.core.IRuntimeView#getConfigurationItems(Object)}.
 *
 * It's able to present the highlight the differences with the previous tree.
 */
public class ConfigurationTreeView<C, T> extends TreeView<ConfigurationItem> {
    BooleanProperty showDifferencesOnly = new SimpleBooleanProperty(false);
    BooleanProperty expandedTree = new SimpleBooleanProperty(false);
    boolean userChangedExpandedStatus = false;
    ContextMenu contextMenu;

    public void setShowDifferencesOnly(boolean showDifferencesOnly) {
        this.showDifferencesOnly.set(showDifferencesOnly);
    }

    public void setExpandedTree(boolean expandedTree) {
        this.expandedTree.set(expandedTree);
    }

    public ConfigurationTreeView(
            ObjectProperty<C> inCurrentConfiguration,
            ObjectProperty<IRuntimeView<C, T>> inRuntimeViewProperty,
            boolean inShowDifferencesOnly,
            boolean inExpandedTree) {
        setShowDifferencesOnly(inShowDifferencesOnly);
        setExpandedTree(inExpandedTree);
        setCellFactory((tV)->new ConfigurationItemTreeCell());

        inCurrentConfiguration.addListener((ov, o, n) -> updateTree(n, inRuntimeViewProperty.get()));
        showDifferencesOnly.addListener((a, o, n) -> {
            setRoot(buildTreeItem(getRoot() == null ? null : getRoot().getValue()));
            setPrefSize(contentWidth(), contentHeight());
        });
        expandedTree.addListener((a, o, n) -> {
            expandCollapseTreeView(getRoot());
            setPrefSize(contentWidth(), contentHeight());
            userChangedExpandedStatus = false;});
        
        this.setOnMouseClicked(me -> {
                setPrefSize(contentWidth(), contentHeight());
        });

        contextMenu = new ContextMenu();

        MenuItem showDifferencesMenuItem = new MenuItem();
        showDifferencesMenuItem.setOnAction((me) -> setShowDifferencesOnly(!showDifferencesOnly.get()));
        showDifferencesMenuItem.textProperty().bind(
                Bindings.createStringBinding(()->showDifferencesOnly.get() ? "Show all items" : "Show only differences", showDifferencesOnly));

        MenuItem expandTreeMenuItem = new MenuItem();
        expandTreeMenuItem.setOnAction((me) -> setExpandedTree(!expandedTree.get()));
        expandTreeMenuItem.textProperty().bind(
                Bindings.createStringBinding(() -> expandedTree.get() ? "Collapse tree" : "Expand tree", expandedTree));

        contextMenu.getItems().addAll(showDifferencesMenuItem, expandTreeMenuItem);


        setOnContextMenuRequested((e) ->
                //we need to get the window, so that the ContextMenu disappears on left click
                //@url(https://bugs.openjdk.java.net/browse/JDK-8095591)
                contextMenu.show(((Node)e.getTarget()).getScene().getWindow(), e.getScreenX(), e.getScreenY()));

        setShowRoot(false);

    }

    public void addContextMenuAction(String iconDescription, StringBinding text, Consumer<Event> value) {
        if (contextMenu == null) return;
        FontIcon icon = new FontIcon(iconDescription);
        MenuItem item = new MenuItem("", icon);
        item.textProperty().bind(text);
        item.setOnAction(value::accept);
        contextMenu.getItems().add(item);
    }

    private void expandCollapseTreeView(TreeItem<?> item){
        if(item != null && !item.isLeaf()){
            if (item != getRoot() || isShowRoot()) {
                item.setExpanded(expandedTree.get());
            }
            for(TreeItem<?> child:item.getChildren()){
                expandCollapseTreeView(child);
            }
        }
    }

    void updateTree(C inConfiguration, IRuntimeView<C, T> inRuntimeView) {
        ConfigurationItem currentConfigurationItem = getRoot() == null ? null : getRoot().getValue();

        if (inConfiguration == null) {
            setRoot(null);
            setPrefSize(contentWidth(), contentHeight());
            return;
        }

        List<ConfigurationItem> configurationItems = inRuntimeView.getConfigurationItems(inConfiguration);
        ConfigurationItem newConfigurationItem = new ConfigurationItem("configuration", inRuntimeView.getConfigurationDescription(inConfiguration), null, configurationItems);
        if (currentConfigurationItem == null) {
            newConfigurationItem.setExpanded(true);
        }

        newConfigurationItem.updateDifferentStatus(currentConfigurationItem);
        newConfigurationItem.updateExpanded(currentConfigurationItem);

        setRoot(buildTreeItem(newConfigurationItem));
        getRoot().setExpanded(newConfigurationItem.isExpanded());

        setPrefSize(contentWidth(), contentHeight());
    }

    TreeItem<ConfigurationItem> buildTreeItem(ConfigurationItem configurationItem) {
        if (configurationItem == null) {
            return null;
        }
        if (configurationItem.getChildren().isEmpty()) {
            return new TreeItem<>(configurationItem);
        }
        TreeItem<ConfigurationItem> subtree = new TreeItem<>(configurationItem);
        subtree.setExpanded((!userChangedExpandedStatus && expandedTree.get()) || configurationItem.isExpanded());
        subtree.expandedProperty().addListener(
                (ao, o, n) -> {
                    userChangedExpandedStatus = true;
                    configurationItem.setExpanded(n);
                });

        Collection<TreeItem<ConfigurationItem>> children = subtree.getChildren();
        for (ConfigurationItem subitem : configurationItem.getChildren()) {
            if (showDifferencesOnly.get() && !subitem.isDifferent()) {
                continue;
            }
            children.add(buildTreeItem(subitem));
        }
        return subtree;
    }

    Map<String, Image> imageMap = new HashMap<>();
    private class ConfigurationItemTreeCell extends TreeCell<ConfigurationItem> {

        @Override
        protected void updateItem(ConfigurationItem item, boolean empty) {
            super.updateItem(item, empty);
            if (empty || item == null) {
                setText(null);
                setGraphic(null);
            }
            else {
                if (item.getIcon() != null && !item.getIcon().isEmpty()) {
                    String iconPath = item.getIcon();
                    Image image = imageMap.get(iconPath);
                    if (image == null) {
                        imageMap.put(iconPath, image = new Image(iconPath, 16,16,true, true));
                    }
                    setGraphic(new ImageView(image));
                }
                setText(item.getName());
                if (item.isDifferent()) {
                    setTextFill(Color.BLUE);
                } else {
                    setTextFill(Color.BLACK);
                }
            }

        }
    }

    public double contentWidth() {
        TreeItem<ConfigurationItem> root = getRoot();
        if (root == null) return 25;
        return computeWidth(root, 0);
    }

    protected double computeWidth(TreeItem<ConfigurationItem> item, int depth) {
        double width = 20 + depth*10 + item.getValue().getName().length() * 8;
        if (item.isExpanded()) {
            for (TreeItem<ConfigurationItem> child : item.getChildren()) {
                width = Math.max(width,computeWidth(child, depth + 1));
            }
        }
        return width;
    }

     double contentHeight() {
        TreeItem<ConfigurationItem> root = getRoot();
        if (root == null) return 15;
        return computeHeight(root, 0) + 15;
    }

     double computeHeight(TreeItem<ConfigurationItem> item, int depth) {
        double height = 20;
        if (depth == 0 && !isShowRoot()) {
            height = 0;
        }

        if (item.isExpanded()) {
            for (TreeItem<ConfigurationItem> child : item.getChildren()) {
                height += computeHeight(child, depth + 1);
            }
        }
        return height;
    }
}

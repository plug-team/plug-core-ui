package plug.ui2.model_view;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

import java.nio.file.Path;
import java.util.Collection;

public class ModelEditor extends BorderPane {
    String name;
    LoadFilePane loadFilePane;
    final ObjectProperty<Path> path;

    public ModelEditor(String name, ObjectProperty<Path> path, BooleanProperty modelChanged, Collection<FileChooser.ExtensionFilter> extensionFilters) {
        this.name = name;
        this.path = path;
        setTop(loadFilePane = new LoadFilePane(name, path, extensionFilters));

        setCenter(new CodeEditor(path, modelChanged));
    }
}

package plug.core.fx;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import javafx.scene.image.Image;

/**
 * This class contains a cache shared images through the application
 */
public class ImageCache {

	private final ImageCache parent;

	private final Map<String, Image> loadedImages = new HashMap<>();

	private Function<String, Image> supplier = (id) -> {
		InputStream stream = getClass().getResourceAsStream(id);
		if (stream == null) return null;
		Image image = new Image(stream);
		return image;
	};

	public ImageCache() {
		this(null);
	}

	public ImageCache(ImageCache parent) {
		this.parent = parent;
	}

	public Function<String, Image> getSupplier() {
		return supplier;
	}

	public void setSupplier(Function<String, Image> supplier) {
		this.supplier = supplier;
	}

	protected boolean isLoaded(String id) {
		return loadedImages.containsKey(id) || (parent != null && parent.isLoaded(id));
	}

	public Image loadImage (String id) {
		if (parent != null && parent.isLoaded(id)) {
			return parent.loadImage(id);
		}

		Image result = loadedImages.get(id);
		if (!loadedImages.containsKey(id)) {
			result = supplier.apply(id);
			loadedImages.put(id, result);
		}
		return result;
	}
}

package plug.ui2.execution_units;

import javafx.scene.control.ListCell;
import plug.core.execution.Execution;
import plug.core.fx.IAppContext;

import java.util.function.Consumer;

public class ExecutionListCell2 extends ListCell<Execution> {
    IAppContext appContext;
    Consumer<Execution> onExecutionError;

    public ExecutionListCell2(
            IAppContext appContext,
            Consumer<Execution> onExecutionError) {
        this.appContext = appContext;
        this.onExecutionError = onExecutionError;
    }

    public void updateItem(Execution execution, boolean empty) {
        super.updateItem(execution, empty);
        setText(null);
        if (empty || execution == null) {
            setGraphic(null);
            return;
        }

        ExecutionView view = new ExecutionView(execution, onExecutionError, appContext);
        setGraphic(view);
    }
}

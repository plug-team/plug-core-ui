package plug.ui2.simulation.expression_watcher;

import javafx.util.StringConverter;

import java.util.List;

class WatchExpressionConverter extends StringConverter<WatchExpression> {
    private final List<WatchExpression> list;
    public WatchExpressionConverter(List<WatchExpression> list) {
        this.list = list ;
    }
    @Override
    public String toString(WatchExpression client) {
        return client.expressionText == null ? "" : client.expressionText;
    }

    @Override
    public WatchExpression fromString(String string) {
        if (string == null || string.length() == 0) {
            return WatchExpression.NO_EXPRESSION;

        }
        return new WatchExpression(string);
    }

}

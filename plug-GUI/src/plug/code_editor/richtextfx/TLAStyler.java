package plug.code_editor.richtextfx;

import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TLAStyler {
    private static final String[] KEYWORDS = new String[]
          { "ASSUME",       "ELSE",      "LOCAL",      "UNION",
            "ASSUMPTION",    "ENABLED",   "MODULE",     "VARIABLE",
            "AXIOM",         "EXCEPT",    "OTHER",      "VARIABLES",
            "CASE",          "EXTENDS",   "SF_",        "WF_",
            "CHOOSE",        "IF",        "SUBSET",     "WITH",
            "CONSTANT",      "IN",        "THEN",
            "CONSTANTS" ,    "INSTANCE",  "THEOREM",
            "DOMAIN",        "LET",       "UNCHANGED"};
    private static final String[] KEYWORDS2 = new String[]{
            "\\\\A", "\\\\E", "\\\\notin",

            "\\\\approx",  "\\\\geq",       "\\\\oslash",     "\\\\sqsupseteq",
            "\\\\asymp",   "\\\\gg",        "\\\\otimes",     "\\\\star",
            "\\\\bigcirc", "\\\\in",        "\\\\preceq",     "\\\\subseteq",
            "\\\\bullet",  "\\\\intersect", "\\\\prec",       "\\\\subset",
            "\\\\cap",     "\\\\land",      "\\\\propto",     "\\\\succeq",
            "\\\\cdot",    "\\\\leq",       "\\\\simeq",      "\\\\succ",
            "\\\\circ",    "\\\\ll",        "\\\\sim",        "\\\\supseteq",
            "\\\\cong",    "\\\\lor",       "\\\\sqcap",      "\\\\supset",
            "\\\\cup",     "\\\\o",         "\\\\sqcup",      "\\\\union",
            "\\\\div",     "\\\\odot",      "\\\\sqsubseteq", "\\\\uplus",
            "\\\\doteq",   "\\\\ominus",    "\\\\sqsubset",   "\\\\wr",
            "\\\\equiv",   "\\\\oplus",     "\\\\sqsupset",

            "!!",  "#",    "##",   "$",    "$$",   "%",    "%%",
            "&",   "&&",   "\\(\\+\\)",  "\\(-\\)",  "\\(\\.\\)",  "\\(/\\)",  "\\(\\\\X\\)",
            "\\*",   "\\*\\*",   "\\+",    "\\+\\+",   "-",    "-\\+->", "--",
            "-\\|",  "\\.\\.",   "\\.\\.\\.",  "/",    "//",   "/=",   "/\\\\",
            "::=", ":=",   ":>",   "<",    "<:",   "<=>",  "=",
            "=<",  "=>",   "=\\|",   ">",    ">=",   "\\?",    "\\?\\?",
            "@@",  "\\\\",    "\\\\/", "\\^",    "\\^\\^",   "\\|",    "\\|-",
            "\\|=",  "\\|\\|",   "~>",   "\\.",

    };

    private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
    private static final String BRACE_PATTERN = "[{}]";
    private static final String BRACKET_PATTERN = "\\[|\\]";
    private static final String SEMICOLON_PATTERN = ";";
    private static final String STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";
    private static final String COMMENT_PATTERN = "\\\\\\*[^\n]*" + "|" + "\\(\\*(.|\\R)*?\\*\\)";

    private static final Pattern PATTERN = Pattern.compile(
            "(?<COMMENT>" + COMMENT_PATTERN + ")"
                    + "|(?<BRACE>" + BRACE_PATTERN + ")"
                    + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
                    + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
                    + "|(?<STRING>" + STRING_PATTERN + ")"
                    + "|(?<KEYWORD>" + KEYWORD_PATTERN + "|" + String.join("|", KEYWORDS2) + ")"
    );

    public StyleSpans<Collection<String>> computeHighlighting(String text) {
        Matcher matcher = PATTERN.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        while (matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                                    matcher.group("BRACE") != null ? "brace" :
                                            matcher.group("BRACKET") != null ? "bracket" :
                                                    matcher.group("SEMICOLON") != null ? "semicolon" :
                                                            matcher.group("STRING") != null ? "string" :
                                                                    matcher.group("COMMENT") != null ? "comment" :
                                                                            null; /* never happens */
            assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }
}
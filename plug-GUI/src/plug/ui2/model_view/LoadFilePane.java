package plug.ui2.model_view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.Collection;

import org.kordamp.ikonli.javafx.FontIcon;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;

public class LoadFilePane extends HBox {
    String name;
    final FontIcon fileSelectIcon = new FontIcon("gmi-find-in-page:18");
    final FontIcon fileClearIcon = new FontIcon("gmi-close:18");

    final ObjectProperty<Path> path;
    final Collection<FileChooser.ExtensionFilter> extensionFilters;
    private SimpleObjectProperty<File> lastKnownDirectoryProperty = new SimpleObjectProperty<File>();
    private File defaultDirectoryFile;
    private String initialDirectory;


    public LoadFilePane(String name, ObjectProperty<Path> path, Collection<FileChooser.ExtensionFilter> extensionFilters) {
        this.name = name;
        this.path = path;
        this.extensionFilters = extensionFilters;

        TextField pathTextField = new TextField();
        StringBinding pathBinding = Bindings.createStringBinding(
                () -> this.path.get() != null ? this.path.get().toString() : "No path selected",
                this.path);

        pathTextField.textProperty().bind(pathBinding);
        Button fileSelectBtn = new Button(null, fileSelectIcon);
        fileSelectBtn.setOnAction(this::selectFilePath);

        Button fileClearBtn = new Button(null, fileClearIcon);
        fileClearBtn.setOnAction((e) -> path.set(null));
        ToolBar toolBar = new ToolBar();
        toolBar.setBackground(Background.EMPTY);
        toolBar.getItems().addAll(fileSelectBtn, fileClearBtn);
        
        // Create directory where to store file containing default paths
        File resDirectory = new File(".runtime-data");
        if(!resDirectory.exists()) {
                resDirectory.mkdir();
        }
        
        // Read the file containing the last path used or create the directory if it does not exist.
        this.defaultDirectoryFile = new File(resDirectory, name + "_default_dir.txt");
        this.initialDirectory = null;
        if(this.defaultDirectoryFile.exists() && this.defaultDirectoryFile.isFile()) {
                try {
                        BufferedReader br = new BufferedReader(new FileReader(this.defaultDirectoryFile));
                        String initialDirectoryRead = br.readLine();
                        br.close();
                        
                        File initialDirectoryFile = new File(initialDirectoryRead);
                        if(initialDirectoryFile.exists() && initialDirectoryFile.isDirectory()) {
                                this.initialDirectory = initialDirectoryRead;   
                        }
                }
                catch (IOException e) {
                        e.printStackTrace();
                }
        }
       


//        ObservableList<ColumnConstraints> constraints = getColumnConstraints();
//        constraints.add(new ColumnConstraints(-1, -1, -1, Priority.NEVER, HPos.RIGHT, false));
//        constraints.add(new ColumnConstraints(100, 200, -1, Priority.ALWAYS, HPos.CENTER, true));
//        constraints.add(new ColumnConstraints(-1));
//        constraints.add(new ColumnConstraints(-1));

//        int column = 0;
//        add(new Label(" " + name+ " "), column++, 0);
//        add(pathTextField, column++, 0);
//        add(toolBar, column++, 0);
//        add(fileSelectBtn, column++, 0);
//        add(fileClearBtn, column++, 0);

        getChildren().addAll(
                new Label(" " + name+ " "),
                pathTextField,
                toolBar);
        setHgrow(pathTextField, Priority.ALWAYS);
        setAlignment(Pos.CENTER);
    }

    public Object selectFilePath( Event evt ) {
        FileChooser chooser = new FileChooser();
        chooser.initialDirectoryProperty().bindBidirectional(this.lastKnownDirectoryProperty);
        if(this.initialDirectory != null && this.lastKnownDirectoryProperty.getValue() == null) {
                chooser.setInitialDirectory(new File(this.initialDirectory));
        }        
        chooser.getExtensionFilters().addAll(extensionFilters);
        chooser.setTitle("Open a " + name);
        File file = chooser.showOpenDialog(getScene().getWindow());
        if (file != null) {
            path.set(file.toPath());
            this.lastKnownDirectoryProperty.setValue(file.getParentFile());
            try {
                    PrintWriter writer = new PrintWriter(this.defaultDirectoryFile);
                    writer.print(file.getParentFile().getCanonicalPath());
                    writer.close();
            }
            catch (IOException e) {
                    e.printStackTrace();
            }
        }
        return null;
    }
}

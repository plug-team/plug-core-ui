package plug.ui2.simulation.ui;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import plug.ui2.AbstractUIBase;

import java.io.IOException;

public class PlugSimulationUI extends AbstractUIBase {

    public PlugSimulationUI() throws IOException { }

    @Override
    public Node getMainComponent() {
        return new SimulationMainView(this);
    }
}

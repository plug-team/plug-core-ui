package plug.wui;

import com.jpro.webapi.JProApplication;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PlugWUI extends JProApplication {


    @Override public void start(Stage stage) {

        FileHandler fileHandler = new FileHandler(getWebAPI());


        Label label = new Label("nothing");
        Button downloadButton = new Button("Download");
        downloadButton.setDisable(true);
        downloadButton.setOnAction(event -> {
            try {
                getWebAPI().downloadURL(fileHandler.fileHandler.getUploadedFile().toURI().toURL());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        fileHandler.fileHandler.progressProperty().addListener((obs,oldV,newV) -> {
            if(newV.doubleValue() == 1.0) {
                downloadButton.setDisable(false);
            }
        });

        VBox root = new VBox(fileHandler, downloadButton, label);
        root.setSpacing(50);
        root.setAlignment(Pos.CENTER);
        Scene scene = new Scene(root, 500, 500);
        scene.getStylesheets().add("/plug/wui/filehandler/filehandler.css");
        stage.setScene(scene);
        stage.show();

    }
}
package plug.ui2.execution_units;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.ILanguagePlugin;
import plug.core.ITransitionRelation;
import plug.core.RuntimeDescription;
import plug.core.execution.ControllerProviderFunction;
import plug.core.execution.Execution;
import plug.core.fx.IAppContext;
import plug.core.fx.verification.ErrorsView;
import plug.core.registry.LanguageModuleRegistry;
import plug.core.ui.common.GPSL2Execution;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ExecutionListView extends SplitPane {
    ObjectProperty<Path> modelPathProperty;
    ObjectProperty<Path> propertiesPathProperty;
    ObservableList<Execution> executionUnits = FXCollections.observableArrayList();

    public ExecutionListView(
            ObjectProperty<Path> modelPath,
            ObjectProperty<Path> propertiesPath,
            BooleanProperty modelChanged,
            BooleanProperty propertiesChanged,
            IAppContext appContext) {
        this.modelPathProperty = modelPath;
        this.propertiesPathProperty = propertiesPath;

        ListView<Execution> executionUnitListView = new ListView<>();
        executionUnitListView.setCellFactory((list) -> new ExecutionListCell2(appContext, this::errorHandler));

        Button runAll = new Button(null, new FontIcon("gmi-fast-forward"));
        runAll.setOnAction((ae) ->
            executionUnitListView.getItems()
                    .forEach(
                            (e) -> Platform.runLater(
                                    () -> {
                                        if (e.status == Execution.Status.CREATED) {
                                            e.initialize();
                                        }
                                        if (e.status != Execution.Status.INITIALIZED) {
                                            e.reset();
                                            e.initialize();
                                        }
                                        e.run();
                                        executionUnitListView.refresh();
                                    })));
        Button exportCSV = new Button(null, new FontIcon("gmi-file-download"));
        exportCSV.setOnAction((ae) -> saveCSV(executionUnitListView.getItems()));

        FlowPane menuFlowPane = new FlowPane();
        menuFlowPane.getChildren().addAll(runAll, exportCSV);

        BorderPane topPane = new BorderPane();

        topPane.setTop(menuFlowPane);
        topPane.setCenter(executionUnitListView);
        this.getItems().add(topPane);

        ErrorsView errorsView = new ErrorsView(appContext, errors);

        //SplitPane bottomLeftPane = new SplitPane(executionUnitsView);
        setOrientation(Orientation.VERTICAL);
        setDividerPosition(0, .7);

        getItems().add(errorsView);
//        errors.addListener((ListChangeListener<? super Throwable>) c -> {
//            ObservableList<Node> items = this.getItems();
//            if (errors.isEmpty()) {
//                items.remove(errorsView);
//            } else {
//                if (!items.contains(errorsView)) {
//                    items.add(errorsView);
//                }
//            }
//        });

        executionUnitListView.setItems(executionUnits);

        //if the model_view path or the properties change -- recreate the execution list
        executionUnitListView.itemsProperty().bind(Bindings.createObjectBinding(this::createExecutionsCallback, modelPath, propertiesPath, modelChanged, propertiesChanged));
    }

    void errorHandler(Execution execution) {
        Throwable exception = execution.getException();
        addError(exception);
    }

    ObservableList<Execution> createExecutionsCallback() {
        errors.clear();
        //stops running executions
        for (Execution execution : executionUnits) {
            execution.stop();
        }
        Path modelPath = modelPathProperty.get();
        if (modelPath == null) return FXCollections.emptyObservableList();

        ILanguagePlugin<ITransitionRelation> languagePlugin = LanguageModuleRegistry.getInstance().getModuleByExtension(modelPath);
        if (languagePlugin == null) return FXCollections.emptyObservableList();

        ObservableList<Execution> executions = FXCollections.observableArrayList();
        try {
            for (Map.Entry<String, ControllerProviderFunction> executionModule : languagePlugin.executions().entrySet()) {
                Execution execution = new Execution(
                        executionModule.getKey(),
                        modelPath,
                        languagePlugin,
                        executionModule.getValue());
                executions.add(execution);
            }

            Path propertiesPath = propertiesPathProperty.get();
            if (propertiesPath == null) return executions;

            RuntimeDescription modelDescription = new RuntimeDescription(modelPath);
            executions.addAll(new GPSL2Execution().executions(propertiesPath, modelDescription));
        } catch (RuntimeException e) {
            addError(e);
        } catch (Throwable e) {
            addError(e);
        }

        return executions;
    }

    void saveCSV(ObservableList<Execution> executions) {
        if (executions == null || executions.isEmpty()) return;
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV File", "*.csv"));
        File file = chooser.showSaveDialog(getScene().getWindow());
        if (file == null) return;

        try {
            Files.write(file.toPath(), toCSV(executions).getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    String toCSV(ObservableList<Execution> executions) {
        List<String[]> table = new ArrayList<>();
        String header[] = {"name", "configurations", "transitions", "loading", "elapsed", "status", "verification", "result"};
        table.add(header);

        for (Execution execution : executions) {
            table.add(executionCSV(execution));
        }
        return table.stream().map((line) -> String.join("; ", line)+"\n").reduce(String::concat).get();

    }
    String[] executionCSV(Execution execution) {
        String result[] = {execution.getName(),
                String.valueOf(execution.configurationCount()),
                String.valueOf(execution.transitionCount()),
                String.valueOf(execution.getLoadingTime()),
                String.valueOf(execution.getElapsedTime()),
                String.valueOf(execution.status()),
                String.valueOf(execution.verificationStatus()),
                String.valueOf(execution.resultStatus())};
        return result;
    }

    protected final ObservableList<Throwable> errors = FXCollections.observableArrayList();
    public void addError(Throwable e) {
        // find root cause
        Throwable root = e;
        while ((root.getCause() != null) && (root = root.getCause()) != null);

        for (Throwable error : errors) {
            if (error.getClass() == root.getClass() && Objects.equals(error.getMessage(), root.getMessage())) {
                // already there
                return;
            }
        }

        errors.add(root);
    }
}

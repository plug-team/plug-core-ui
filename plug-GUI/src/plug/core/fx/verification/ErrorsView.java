package plug.core.fx.verification;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import org.fxmisc.richtext.CodeArea;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.fx.IAppContext;


public class ErrorsView extends TableView<Throwable> {

	protected final IAppContext context;

	public ErrorsView(IAppContext context, ObservableList<Throwable> errors) {
		this.context = context;

		TableColumn<Throwable, Throwable> iconColumn = new TableColumn<>("");
		iconColumn.setCellValueFactory((c) -> new SimpleObjectProperty<>(c.getValue()));
		iconColumn.setCellFactory((c) -> new IconCell());
		iconColumn.setPrefWidth(25);
		getColumns().add(iconColumn);

		TableColumn<Throwable, Class> typeColumn = new TableColumn<>("Type");
		typeColumn.setCellValueFactory(new PropertyValueFactory<>("class"));
		typeColumn.setCellFactory((c) -> new ErrorCell());
		typeColumn.setPrefWidth(110);
		getColumns().add(typeColumn);

		TableColumn<Throwable, String> messageColumn = new TableColumn<>("Message");
		messageColumn.setCellValueFactory(new PropertyValueFactory<>("message"));
		messageColumn.setCellFactory((c) -> new MessageCell());
		messageColumn.prefWidthProperty().bind(widthProperty().subtract(typeColumn.widthProperty()).subtract(iconColumn.widthProperty()));
		getColumns().add(messageColumn);

		setItems(errors);
	}

	public class IconCell extends TableCell<Throwable, Throwable> {

		protected final FontIcon icon = new FontIcon("gmi-error:20:red");

		public IconCell() {
			icon.setOnMouseClicked(event -> {
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(stream));
				getItem().printStackTrace(writer);
				writer.close();
				CodeArea code = new CodeArea(new String(stream.toByteArray(), StandardCharsets.UTF_8));
				code.setEditable(false);
				context.openView("Error " + getItem().getMessage(), code);
			});
		}

		@Override
		protected void updateItem(Throwable item, boolean empty) {
			super.updateItem(item, empty);
			setText(null);
			if (empty ) {
				setGraphic(null);
			} else {
				setGraphic(icon);
			}
		}
	}


	public class ErrorCell extends TableCell<Throwable, Class> {
		@Override
		protected void updateItem(Class item, boolean empty) {
			super.updateItem(item, empty);
			setGraphic(null);
			if (empty || item == null) {
				setText(null);
			} else {
				String simpleName = item.getSimpleName();
				simpleName = simpleName.replaceAll("[Ee]xception", "");
				setText(simpleName);
			}
		}
	}

	public class MessageCell extends TableCell<Throwable, String> {
		@Override
		protected void updateItem(String item, boolean empty) {
			super.updateItem(item, empty);
			setGraphic(null);
			if (empty || item == null) {
				setText(null);
			} else {
				String text = item.replaceAll("\\[.*Exception\\] *", "");
				setText(text);
			}
		}
	}

	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
		Pane header = (Pane) lookup("TableHeaderRow");
		header.setPrefHeight(15);
	}
}

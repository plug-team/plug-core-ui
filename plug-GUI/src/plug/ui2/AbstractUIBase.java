package plug.ui2;

import java.io.IOException;
import java.net.URL;
import java.util.function.Consumer;

import org.controlsfx.control.StatusBar;
import org.kordamp.ikonli.javafx.FontIcon;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import plug.core.fx.IAppContext;
import plug.utils.file.FileWatcher;
import plug.utils.ui.fx.GuiFxUtils;

public abstract class AbstractUIBase extends Application implements IAppContext {
    FileWatcher fileWatcher = new FileWatcher();
    Stage stage;
    public Scene mainScene;
    private static boolean firstDisplay = true;

    public AbstractUIBase() throws IOException {
    }

    @Override
    public FileWatcher getFileWatcher() {
        return fileWatcher;
    }

    @Override
    public void openView(String title, Region view, Consumer<Region> onClose) {
        BorderPane mainPane = new BorderPane();
        mainPane.setCenter(view);

        FontIcon closeIcon = new FontIcon("gmi-close:20");
        GuiFxUtils.addButtonEffects(closeIcon);
        
        closeIcon.setOnMouseClicked(event -> {
            boolean isMaximized = stage.isMaximized();
            if(firstDisplay) {
                    stage.setMaximized(true);
                    stage.setMaximized(false);
                    firstDisplay = false;
            }
            stage.setScene(mainScene);
            mainScene.getWindow().setHeight(stage.getHeight());
            mainScene.getWindow().setWidth(stage.getWidth());
            if(isMaximized) {
                    stage.setMaximized(true);
                    firstDisplay = false;  
            }
            if (onClose != null) {
                onClose.accept(view);
            }
        });
        Text titleText = new Text(title);
        titleText.setTextAlignment(TextAlignment.CENTER);

        BorderPane titlePane = new BorderPane( titleText );
        titlePane.setLeft(closeIcon);
        mainPane.setTop(titlePane);

        Scene theScene = new Scene(buildMain(mainPane), mainScene.getWidth(), mainScene.getHeight());
        addCSS(theScene);
        stage.setScene(theScene);
    }

    public void addCSS(Scene scene) {
        // try {
        URL css = getClass().getResource("/plug/ui2/style.css");
        //Path path = Paths.get(css.toURI());
        GuiFxUtils.setStylesheet(
                css,
                scene,
                fileWatcher
        );
        // } catch (URISyntaxException e) { }
    }

    Scene theScene = new Scene(new OutputRedirect());
    final ObjectProperty<Stage> theErrorStage = new SimpleObjectProperty<>();

    void showErrorHandler(MouseEvent me) {
        Stage theStage = theErrorStage.get();
        if (theStage == null) {
            theErrorStage.set(theStage = new Stage());
            theStage.setScene(theScene);
            theStage.setTitle("Console Messages");
        }

        theStage.show();
        theStage.requestFocus();
    }
    public StatusBar getStatusBar() {
        StatusBar statusBar = new StatusBar();
        statusBar.setText("");
        FontIcon errorIcon = new FontIcon("gmi-error-outline:20:#FF0000");
        errorIcon.setOnMouseClicked(this::showErrorHandler);
        statusBar.getRightItems().addAll(errorIcon);
        return statusBar;
    }

    public abstract Node getMainComponent();

    @Override
    public void init() throws Exception {
        super.init();

        mainScene = new Scene(buildMain(getMainComponent()), 800, 600);
    }

    Parent buildMain(Node node) {
        return new BorderPane(node, null, null, getStatusBar(), null);
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });

        addCSS(mainScene);
        stage.setScene(mainScene);
        stage.show();
    }

}

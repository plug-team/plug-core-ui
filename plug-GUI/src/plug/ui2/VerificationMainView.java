package plug.ui2;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.controlsfx.control.StatusBar;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.ILanguagePlugin;
import plug.core.fx.IAppContext;
import plug.core.registry.LanguageModuleRegistry;
import plug.ui2.execution_units.ExecutionListView;
import plug.ui2.model_view.ModelEditor;
import plug.utils.file.FileWatcher;


import java.io.*;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VerificationMainView extends VBox {
    ObjectProperty<Path> modelPathProperty = new SimpleObjectProperty<>();
    ObjectProperty<Path> propertiesPathProperty = new SimpleObjectProperty<>();
    BooleanProperty modelChanged = new SimpleBooleanProperty(false);
    BooleanProperty propertiesChanged = new SimpleBooleanProperty(false);
    FileWatcher watcher;

    public VerificationMainView(Path modelPath, Path propertiesPath, IAppContext appContext) {
        if (modelPath != null) {
            modelPathProperty.setValue(modelPath);
        }
        if (propertiesPath != null) {
            propertiesPathProperty.setValue(propertiesPath);
        }
        watcher = appContext.getFileWatcher();

        modelPathProperty.addListener(this::modelChangedCallback);
        propertiesPathProperty.addListener(this::propertiesChangedCallback);

        ModelEditor modelUnderVerificationView = new ModelEditor(
                "Model",
                modelPathProperty,
                modelChanged,
                modelExtensionFilters());
        ModelEditor propertiesView = new ModelEditor(
                "Properties",
                propertiesPathProperty,
                propertiesChanged,
                Collections.singleton(new FileChooser.ExtensionFilter("GPSL", "*.gpsl")));

        ExecutionListView executionListView = new ExecutionListView(
                modelPathProperty,
                propertiesPathProperty,
                modelChanged,
                propertiesChanged,
                appContext);

        SplitPane bottomPane = new SplitPane();
        bottomPane.setOrientation(Orientation.HORIZONTAL);
        bottomPane.getItems().addAll(executionListView, propertiesView);

        SplitPane mainView = new SplitPane(modelUnderVerificationView, bottomPane);
        mainView.setOrientation(Orientation.VERTICAL);

        getChildren().addAll(mainView);
    }

    public void modelChangedCallback(ObservableValue<? extends Path> obj, Path oldP, Path newP) {
        watcher.removePathListener(oldP);
        watcher.addOnContentChange(newP, (c) -> Platform.runLater(() -> modelChanged.set(!modelChanged.get())));
    }

    public void propertiesChangedCallback(ObservableValue<? extends Path> obj, Path oldP, Path newP) {
        watcher.removePathListener(oldP);
        watcher.addOnContentChange(newP, (c) -> Platform.runLater(() -> propertiesChanged.set(!propertiesChanged.get())));
    }

    public Collection<FileChooser.ExtensionFilter> modelExtensionFilters () {
        List<FileChooser.ExtensionFilter> extensionFilters = new ArrayList<>();
        for (ILanguagePlugin languageModule : LanguageModuleRegistry.getInstance().getModules()) {
            if (languageModule.getName().equals("Buchi")) {continue;}
            List<String> extensions = Stream.of(languageModule.getExtensions()).map(e -> "*" + e).collect(Collectors.toList());
            extensionFilters.add(new FileChooser.ExtensionFilter(languageModule.getName(), extensions));
        }
        return extensionFilters;
    }
}

package plug.ui2.simulation.expression_watcher;

import javafx.beans.property.ObjectProperty;
import javafx.event.EventTarget;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.ITransitionRelation;

import java.util.List;
import java.util.function.Consumer;

public class ExpressionWatcherTitledPane<C, T> extends TitledPane {

    public ExpressionWatcherTitledPane(
            ObjectProperty<C> inCurrentConfiguration,
            ObjectProperty<ITransitionRelation<C, T>> inTransitionRelationProperty,
            Consumer<List<C>> counterExampleHandler) {

        ExpressionsWatcher<C, T> watcher =
                new ExpressionsWatcher<>(
                        inCurrentConfiguration,
                        inTransitionRelationProperty,
                        counterExampleHandler);

        FontIcon addWatchIcon =  new FontIcon("gmi-add:18:#2ecc71");
        addWatchIcon.setOnMouseClicked(e -> {
            System.out.println("add-watch");
            int idx = watcher.listView.getItems().indexOf(WatchExpression.NO_EXPRESSION);
            watcher.listView.edit(idx);
        });
        FontIcon deleteWatchIcon = new FontIcon("gmi-delete-forever:18:#2ecc71");
        deleteWatchIcon.setOnMouseClicked(e -> watcher.theWatchExpList.retainAll(WatchExpression.NO_EXPRESSION));

        HBox watchToolbar = new HBox(addWatchIcon, deleteWatchIcon);

        setText("Watch");
        setContent(watcher);
        setGraphic(watchToolbar);
        setExpanded(false);
        //filter the mouse released event so that the titlearea does not collapse when clicking the button
        addEventFilter(MouseEvent.MOUSE_RELEASED, e -> {
            EventTarget target = e.getTarget();
            if (target.equals(addWatchIcon) || target.equals(deleteWatchIcon)) {
                e.consume();
            }
        });
    }
}

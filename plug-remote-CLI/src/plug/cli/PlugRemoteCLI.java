package plug.cli;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionHandlerFilter;
import plug.core.ILanguagePlugin;
import plug.core.ITransitionRelation;
import plug.core.RuntimeDescription;
import plug.language.remote.RemoteLoader;
import plug.language.remote.RemotePlugin;
import plug.language.remote.runtime.RemoteRuntime;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.util.Map;

public class PlugRemoteCLI {

    static CmdLineParser parser;

    public static void main(String[] args) {
        VerificationRemoteMain main = new VerificationRemoteMain();
        parser = new CmdLineParser(main);
        try {
            parser.parseArgument(args);
            main.run();
        } catch (Throwable e) {
            System.err.println(e.getMessage());
            printHelp(System.err);
            System.exit(1);
        }
    }

    public static void printHelp(PrintStream printStream) {
        String className = MethodHandles.lookup().lookupClass().getCanonicalName();
        printStream.println("java " + className + " [options...] arguments...");
        // print the list of available options
        parser.printUsage(printStream);
        printStream.println();

        // print option sample. This is useful some times
        printStream.println("  Example: java " + className + " " + parser.printExample(OptionHandlerFilter.REQUIRED) + " <model-path>");
        System.exit(1);
    }
}

class VerificationRemoteMain extends VerificationMain {
    @Option(name = "-s", aliases = {"--language-server"}, usage = "The remote language server:port")
    String languageServer;

    public void getHelp() {
        PlugRemoteCLI.printHelp(System.out);
    }

    public ILanguagePlugin<ITransitionRelation> getLanguagePlugin() {

        if (languageServer == null && modelPath == null) {
            PlugRemoteCLI.printHelp(System.out);
            return null;
        }

        if (languageServer != null) {
            String address = languageServer.substring(0, languageServer.lastIndexOf(':'));
            int port = Integer.parseInt(languageServer.substring(languageServer.lastIndexOf(':') + 1));

            System.out.println(address + " : " + port);

            RemotePlugin languagePlugin = new RemotePlugin(new RemoteLoader() {
                public RemoteRuntime getRuntime(URI modelURI, Map<String, Object> options) throws IOException {
                    RemoteRuntime runtime = new RemoteRuntime(address, port);
                    // TODO check when the runtime should be initialized
                    runtime.initializeRuntime();
                    return runtime;
                }
            });
            return (ILanguagePlugin) languagePlugin;
        }
        //if no language server default on the modelPath
        return super.getLanguagePlugin();
    }

    public RuntimeDescription getRuntimeDescription() {
        if (languageServer == null && modelPath == null) {
            PlugRemoteCLI.printHelp(System.out);
            return null;
        }
        if (languageServer != null) {
            ILanguagePlugin plugin = getLanguagePlugin();
            return new RuntimeDescription(plugin, () -> {
                try {
                    return plugin.getLoader().getRuntime((URI) null, null);
                } catch (Exception e) {
                    System.err.println("Could not load the languagePlugin for " + languageServer);
                    e.printStackTrace();
                }
                return null;
            });
        }
        //if no language server default on the modelPath
        return super.getRuntimeDescription();
    }
}
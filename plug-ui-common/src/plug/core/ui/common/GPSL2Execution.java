package plug.core.ui.common;

import org.antlr.v4.runtime.ANTLRFileStream;
import plug.core.ILanguagePlugin;
import plug.core.RuntimeDescription;
import plug.core.execution.Execution;
import plug.explorer.buchi.nested_dfs.BA_GaiserSchwoon_Iterative;
import plug.language.buchi.runtime.BuchiRuntime;
import plug.language.buchikripke.runtime.KripkeBuchiLoader;
import plug.language.buchikripke.runtime.KripkeBuchiPlugin;
import plug.language.buchikripke.runtime.KripkeBuchiProductSemantics;
import plug.language.buchikripke.sk_tba.SK_TBA_Loader;
import plug.language.buchikripke.sk_tba.SK_TBA_Plugin;
import plug.language.buchikripke.sk_tba.SK_TBA_ProductSemantics;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.BuchiAutomata.analysis.AcceptAllExtractor;
import properties.LTL.transformations.LTL2Buchi;

import java.io.PrintWriter;
import java.net.URI;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class GPSL2Execution {
	public Collection<Execution> executions(Path propertiesPath, RuntimeDescription modelDescription) throws Exception {
		ANTLRFileStream is = new ANTLRFileStream(propertiesPath.toAbsolutePath().toString());

		LTL2Buchi convertor = new LTL2Buchi(new PrintWriter(System.out));
		Collection<BuchiDeclaration> buchiDeclarations = convertor.getBuchiDeclarations(is);

		ArrayList<Execution> executions = new ArrayList<>();
		for (BuchiDeclaration buchi : buchiDeclarations) {
			Execution execution = getExecution(modelDescription, buchi, Execution.TransitionStorageType.PARENT);
			executions.add(execution);
		}
		return executions;
	}

	public Execution execution(String name, Path propertiesPath, RuntimeDescription modelDescription, Execution.TransitionStorageType transitionStorageType) throws Exception {
		ANTLRFileStream is = new ANTLRFileStream(propertiesPath.toAbsolutePath().toString());

		LTL2Buchi converter = new LTL2Buchi(new PrintWriter(System.out));
		BuchiDeclaration buchi = converter.getBuchiDeclaration(name, is);
		return getExecution(modelDescription, buchi, transitionStorageType);
	}

	private Execution getExecution(RuntimeDescription modelDescription, BuchiDeclaration buchi, Execution.TransitionStorageType transitionStorageType) {
		AcceptAllExtractor aae = new AcceptAllExtractor();
		Set<Object> acceptAllSet = aae.getAcceptAllSet(buchi);
		Execution execution = new Execution(
				buchi.getName(),
				modelDescription.modelPath,
				sk_tba_plugin(
						modelDescription,
						new RuntimeDescription(
								null,
								() -> new BuchiRuntime(buchi))),
				BA_GaiserSchwoon_Iterative::new,
				transitionStorageType,
				acceptAllSet);
		return execution;
	}



	//instantiates the kripke-buchi product semantics without buchi2kripke transformation
	ILanguagePlugin sk_tba_plugin(RuntimeDescription modelDescription, RuntimeDescription buchiDescription) {
		//System.out.println("uses the SK_TBA product semantics");
		SK_TBA_Loader loader = new SK_TBA_Loader() {
			@Override
			public SK_TBA_ProductSemantics getRuntime(URI modelURI, Map<String, Object> options) throws Exception {
				return new SK_TBA_ProductSemantics(modelDescription, buchiDescription);
			}
		};
		SK_TBA_Plugin plugin = new SK_TBA_Plugin() {
			@Override
			public SK_TBA_Loader getLoader() {
				return loader;
			}
		};
		return plugin;
	}

	//instantiates the kripke-buchi product semantics that uses the buchi2kripke transformations
	ILanguagePlugin b2k_plugin(RuntimeDescription modelDescription, RuntimeDescription buchiDescription) {
		//System.out.println("uses the Buchi to Kripke product semantics");
		KripkeBuchiLoader loader = new KripkeBuchiLoader() {
			@Override
			public KripkeBuchiProductSemantics getRuntime(URI modelURI, Map<String, Object> options) throws Exception {
				return new KripkeBuchiProductSemantics(modelDescription, buchiDescription);
			}
		};
		KripkeBuchiPlugin plugin = new KripkeBuchiPlugin() {
			@Override
			public KripkeBuchiLoader getLoader() {
				return loader;
			}
		};
		return plugin;
	}
}


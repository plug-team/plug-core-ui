package plug.ui2.simulation.fireables;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.layout.StackPane;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.core.IFiredTransition;
import plug.core.IRuntimeView;
import plug.core.ITransitionRelation;
import plug.simulation.trace_storage.TraceEntry;
import plug.simulation.trace_storage.TraceStore;
import plug.utils.ui.fx.GuiFxUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ciprian on 19/02/17.
 */
public class FireableTransitionsView<C, T> extends StackPane {
    private final ObjectProperty<ITransitionRelation<C, T>> transitionRelationProperty;
    private final ObjectProperty<IFiredTransition<C,?>> outFiredTransition;
    protected final Random rnd = new Random();
    //ObjectProperty<C> currentConfiguration;
    ObjectProperty<TraceEntry<C, Object>> currentEntry;
    protected  ListProperty<T> fireableTransitions = new SimpleListProperty<>();
    protected final ListView<T> listView;
    ObjectProperty<TraceStore<C, Object>> traceStore;

    public FireableTransitionsView(
            ObjectProperty<TraceEntry<C, Object>> inCurrentEntry,
            ObjectProperty<ITransitionRelation<C, T>> inTransitionRelationProperty,
            ObjectProperty<IRuntimeView<C, T>> inRuntimeViewProperty,
            ObjectProperty<IFiredTransition<C,?>> outFiredTransition,
            ObjectProperty<TraceStore<C, Object>> inTraceStore) {

        this.currentEntry = inCurrentEntry;
        this.transitionRelationProperty = inTransitionRelationProperty;
        this.outFiredTransition = outFiredTransition;
        this.traceStore = inTraceStore;

        this.setBackground(Background.EMPTY);

        fireableTransitions.bind(Bindings.createObjectBinding(this::fireableTransitionsFromCallback, currentEntry));
        SetProperty<T> alreadyFiredSet = new SimpleSetProperty<>();
        alreadyFiredSet.bind(Bindings.createObjectBinding(this::alreadyFiredCallback, currentEntry, fireableTransitions));

        Button stepRandomButton = new Button("", new FontIcon("typ-arrow-shuffle:20:darkgreen"));
        stepRandomButton.setPickOnBounds(false);
        stepRandomButton.setId("fireRandom");

        stepRandomButton.setTooltip(new Tooltip("Fire Random Transition"));
        GuiFxUtils.addButtonEffects(stepRandomButton);
        stepRandomButton.setOnAction(event -> {
            int numberOfFireables = fireableTransitions.size();
            if (numberOfFireables < 1) return;
            int index = rnd.nextInt(numberOfFireables);
            fireCallback(fireableTransitions.get(index));
        });
        fireableTransitions.addListener(
                (a, o, n) -> {
                    if (n==null || n.isEmpty()) {
                        stepRandomButton.setDisable(true);
                    } else {
                        stepRandomButton.setDisable(false);
                    }
                });

        listView = new ListView<>(fireableTransitions);
        listView.setCellFactory(v -> new FireableTransitionCell<>(this::fireCallback, inRuntimeViewProperty, alreadyFiredSet));

        listView.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                fireCallback(listView.getSelectionModel().getSelectedItem());
            }
        });

        getChildren().addAll(listView,stepRandomButton);
        setAlignment(stepRandomButton, Pos.CENTER_RIGHT);
    }

    private ObservableList<T> fireableTransitionsFromCallback() {
        ITransitionRelation<C, T> transitionRelation = transitionRelationProperty.get();
        TraceEntry<C, Object> entry = currentEntry.get();
        if (entry == null) return FXCollections.emptyObservableList();
        C source = entry.getConfiguration();
        if (source == null) return FXCollections.emptyObservableList();
        Collection<T> fireables = transitionRelation.fireableTransitionsFrom(source);

        //if the runtime has blocking transitions we should prune the fireable list by removing the ones that will block during execution
        if (transitionRelation.hasBlockingTransitions()) {
            Iterator<T> iterator = fireables.iterator();
            T fireable;
            while (iterator.hasNext()){
                fireable = iterator.next();
                IFiredTransition<C, ?> fired = transitionRelation.fireOneTransition(source, fireable);
                if (fired == null || fired.getTargets().isEmpty()) {
                    iterator.remove();
                }
            }
        }

        return FXCollections.observableArrayList(fireables);
    }

    private ObservableSet<T> alreadyFiredCallback() {
        TraceEntry<C, Object> entry = currentEntry.get();
        if (entry == null) return FXCollections.emptyObservableSet();
        Set<T> set = fireableTransitions.stream()
                .filter(fireable -> {
                    for (TraceEntry<C, Object> child : entry.getChildren()) {
                        if (fireable.equals(child.getAction())) {
                            return true;
                        }
                    }
                    return false;
                })
                .collect(Collectors.toSet());
        return FXCollections.observableSet(set);
    }

    private void fireCallback(T transition) {
        ITransitionRelation<C, T> transitionRelation = transitionRelationProperty.get();
        if (currentEntry.get() == null || transition == null) return;
        outFiredTransition.set( transitionRelation.fireOneTransition(currentEntry.get().getConfiguration(),transition) );
    }
}


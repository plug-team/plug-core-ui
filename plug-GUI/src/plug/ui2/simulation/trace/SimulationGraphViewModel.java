package plug.ui2.simulation.trace;

import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import plug.core.IFiredTransition;
import plug.core.IRuntimeView;
import plug.core.ITransitionRelation;
import plug.simulation.trace_storage.TraceEntry;
import plug.utils.ui.graph.GraphViewModel;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Deprecated
public class SimulationGraphViewModel<C, T> extends GraphViewModel<TraceEntry<C, Object>, Object> {
//TODO: this class should not exist

	private final ObjectProperty<ITransitionRelation<C, T>> transitionRelationProperty;
	private final ObjectProperty<IRuntimeView<C, T>> runtimeViewProperty;
    Function<TraceEntry<C, T>, Node> configurationViewBuilder;


	private boolean allColorsConsumed = false;

	/** Constructs a stream of colors declared in Color class */
	private final List<Color> colors = new ArrayList<>(
			Arrays.stream(Color.class.getDeclaredFields())
					.filter(f -> Modifier.isStatic(f.getModifiers()) && f.getType() == Color.class)
					.map(f -> { try { return (Color) f.get(Color.class); } catch (IllegalAccessException e) { return null; }})
					.filter(Objects::nonNull).filter(Color::isOpaque)
					.filter(color -> color.getSaturation() > 0.2 && color.getSaturation() < 0.7)
					.collect(Collectors.toList())
			);

	/** Iterates over colors with an index to avoid concurrent modification while recycling colors */
	private int colorIndex = 0;

	private final ObservableMap<Object, Integer> configurationCounts = FXCollections.observableHashMap();
	private final Map<Object, Color> colorMap = new HashMap<>();

	public SimulationGraphViewModel(
			ObjectProperty<ITransitionRelation<C, T>> transitionRelationProperty,
			ObjectProperty<IRuntimeView<C, T>> runtimeViewProperty,
			Function<TraceEntry<C, T>, Node> configurationViewBuilder) {
		this.transitionRelationProperty = transitionRelationProperty;
		this.runtimeViewProperty = runtimeViewProperty;
		this.configurationViewBuilder = configurationViewBuilder;
		getInitialVertices().addListener(this::onInitialChanged);
		getEdges().addListener(this::onEdgesChanged);
		configurationCounts.addListener(this::onConfigurationCounts);
	}

	@Override
	public String vertexDescription(TraceEntry<C, Object> vertex) {
		return runtimeViewProperty.get().getConfigurationDescription(vertex.getConfiguration());
	}

	@Override
	public String edgeDescription(Object edge) {
		return runtimeViewProperty.get().getActionDescription(edge);
	}

	@Override
	public Region vertexNode(TraceEntry<C, Object> vertex) {
		if (configurationViewBuilder == null) return new Label("no view builder");
		return (Region)configurationViewBuilder.apply((TraceEntry<C, T>) vertex);
	}

	@Override
	public boolean vertexIsSink(TraceEntry<C, Object> vertex) {
		ITransitionRelation<C, T> transitionRelation = transitionRelationProperty.get();
		Collection<T> fireables = transitionRelation.fireableTransitionsFrom(vertex.getConfiguration());

		//if the runtime has blocking transitions we should prune the fireable list by removing the ones that will block during execution
		if (transitionRelation.hasBlockingTransitions()) {
			Iterator<T> iterator = fireables.iterator();
			T fireable;
			while (iterator.hasNext()){
				fireable = iterator.next();
				IFiredTransition<C, ?> fired = transitionRelation.fireOneTransition(vertex.getConfiguration(), fireable);
				if (fired == null || fired.getTargets().isEmpty()) {
					iterator.remove();
				}
			}
		}
		return fireables.isEmpty();
	}

	/** Color for vertex */
	@Override
	public Color vertexColor(TraceEntry<C, Object> vertex) {
		return allColorsConsumed ? Color.WHITE : colorMap.getOrDefault(vertex.getConfiguration(), Color.WHITE);
	}

	protected void onInitialChanged(ListChangeListener.Change<? extends TraceEntry<C, Object>> change) {
		change.reset();
		while (change.next()) {
			for (TraceEntry<C, Object> entry : change.getAddedSubList()) {
				count(entry.getConfiguration());
			}
			for (TraceEntry<C, Object> traceEntry : change.getRemoved()) {
				decount(traceEntry.getConfiguration());
			}
		}
	}

	protected void onEdgesChanged(ListChangeListener.Change<? extends Edge<TraceEntry<C, Object>, Object>> change) {
		if (!allColorsConsumed) {
			change.reset();
			while (change.next()) {
				for (Edge<TraceEntry<C, Object>, Object> pair : change.getAddedSubList()) {
					count(pair.target.getConfiguration());
				}
				for (Edge<TraceEntry<C, Object>, Object> pair : change.getRemoved()) {
					decount(pair.target.getConfiguration());
				}
			}
		}
	}

	protected void count(Object configuration) {
		configurationCounts.put(configuration, configurationCounts.getOrDefault(configuration, 0) + 1);
	}

	protected void decount(Object configuration) {
		Integer count = configurationCounts.getOrDefault(configuration, 0);
		if (count <= 1) {
			configurationCounts.remove(configuration);
		} else {
			configurationCounts.put(configuration, count - 1);
		}
	}

	protected void onConfigurationCounts(MapChangeListener.Change<? extends Object, ? extends Integer> change) {
		if (change.wasAdded()) {
			Object key = change.getKey();
			if (change.getValueAdded() >= 2) {
				if (!colorMap.containsKey(key)) {
					// passed from 1 to 2
					newColorForConfiguration(key);
				}
			} else if (change.getValueAdded() == 1) {
				// passed from 2 to 1
				Color removed = colorMap.remove(key);
				if (removed != null) {
					// recycles color
					colors.add(colorIndex, removed);
				}
			}
		}
	}

	protected void newColorForConfiguration(Object configuration) {
		if (colorIndex < colors.size()) {
			colorMap.put(configuration, colors.get(colorIndex));
			colorIndex += 1;
		}
		else {
			allColorsConsumed = true;
			colorMap.clear();
		}
	}

    @Override
    public Set<TraceEntry<C, Object>> equivalentVertices(TraceEntry<C, Object> vertex) {
		//TODO: if this is usefull it should not be implemented like this
        Set<TraceEntry<C, Object>> equivalent = new HashSet<>();
        for (TraceEntry<C, Object> entry : initialVertices) {
            if (vertex != entry) {
                if (entry.getConfiguration().equals(vertex.getConfiguration())) {
                    equivalent.add(entry);
                }
            }
        }
        for (Edge<TraceEntry<C, Object>, Object> edge : edges) {
            if (vertex != edge.target) {
                if (edge.target.getConfiguration().equals(vertex.getConfiguration())) {
                    equivalent.add(edge.target);
                }
            }
        }
        return equivalent;
    }
}

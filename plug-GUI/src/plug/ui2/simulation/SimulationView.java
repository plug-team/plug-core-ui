package plug.ui2.simulation;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.Event;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.VBox;
import plug.core.IFiredTransition;
import plug.core.IRuntimeView;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.events.PropertyEvent;
import plug.folding.runtime.PreinitializedRuntime;
import plug.simulation.trace_storage.TraceEntry;
import plug.simulation.trace_storage.TraceStore;
import plug.statespace.SimpleStateSpaceManager;
import plug.ui2.simulation.configuration.ConfigurationTreeView;
import plug.ui2.simulation.configuration.ConfigurationTreeViewTitledPane;
import plug.ui2.simulation.expression_watcher.ExpressionWatcherTitledPane;
import plug.ui2.simulation.fireables.FireableTransitionsView;
import plug.ui2.simulation.trace.BranchingTraceView;
import plug.utils.graph.algorithms.DijkstraShortestPath;
import plug.verifiers.deadlock.DeadlockVerifier;
import plug.verifiers.deadlock.FinalStateDetected;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

public class SimulationView<C, T> extends SplitPane {
    ObjectProperty<ITransitionRelation<C, T>> transitionRelationProperty;
    ObjectProperty<IRuntimeView<C, T>> runtimeViewProperty;
    ObjectProperty<C> theCurrentConfiguration = new SimpleObjectProperty<>();
    ObjectProperty<TraceEntry<C, Object>> theCurrentEntry = new SimpleObjectProperty<>();

    BranchingTraceView<C, T> branchingTraceView;

    public SimulationView(ObjectProperty<ITransitionRelation<C, T>> transitionRelation,
                          ObjectProperty<IRuntimeView<C, T>> runtimeView,
                          ObjectProperty<Set<C>> initialSet) {
        this(transitionRelation, runtimeView, initialSet, new SimpleObjectProperty<>());
    }

    public SimulationView(
            ObjectProperty<ITransitionRelation<C, T>> transitionRelationProperty,
            ObjectProperty<IRuntimeView<C, T>> runtimeViewProperty,
            ObjectProperty<Set<C>> initialSet,
            ObjectProperty<TraceStore<C, Object>> inoutTraceStoreProperty) {

        this.transitionRelationProperty = transitionRelationProperty;
        this.runtimeViewProperty = runtimeViewProperty;
        ObjectProperty<IFiredTransition<C,?>> theFiredTransition = new SimpleObjectProperty<>();

        FireableTransitionsView<C, T> fireableTransitionsView =
                new FireableTransitionsView<>(
                        theCurrentEntry,
                        transitionRelationProperty,
                        runtimeViewProperty,
                        theFiredTransition,
                        inoutTraceStoreProperty
                );

        ConfigurationTreeViewTitledPane<C, T> configurationTitledPane =
                new ConfigurationTreeViewTitledPane<>(
                        theCurrentConfiguration,
                        runtimeViewProperty
                );
        configurationTitledPane.addToolbarAction("gmi-keyboard-tab:18:#2ecc71", Bindings.createStringBinding(() -> "Find deadlock"), this::deadlockFinder);

        ExpressionWatcherTitledPane<C, T> watchTitledPane =
                new ExpressionWatcherTitledPane<>(
                        theCurrentConfiguration,
                        transitionRelationProperty,
                        this::addCounterExample
                );

        //cannot use accordion because it allows for only one TitlePane to be open
        final VBox accordion = new VBox (watchTitledPane, configurationTitledPane);
        ScrollPane accordionScrollPane = new ScrollPane(accordion);
        accordionScrollPane.setFitToWidth(true);

        SplitPane topPane = new SplitPane(fireableTransitionsView, accordionScrollPane);

        branchingTraceView = new BranchingTraceView<>(
                transitionRelationProperty,
                runtimeViewProperty,
                theFiredTransition,
                //theCurrentConfiguration,
                inoutTraceStoreProperty,
                theCurrentEntry,
                this::configurationViewBuilder);
        getItems().addAll(topPane, branchingTraceView);

        setOrientation(Orientation.VERTICAL);
        setDividerPosition(0, .3);

        //set the initial configurations
        Set<C> initialConfigurations = getInitialConfigurations(initialSet.get(), transitionRelationProperty.get());
        branchingTraceView.setInitialConfigurations(initialConfigurations);

        initialSet.addListener((v, o, n) -> {
            Set<C> initials = getInitialConfigurations(initialSet.get(), transitionRelationProperty.get());
            branchingTraceView.setInitialConfigurations(initials);
        });

        theCurrentConfiguration.bind(Bindings.createObjectBinding( () -> theCurrentEntry.get().getConfiguration(), theCurrentEntry ));
    }

    Node configurationViewBuilder(TraceEntry<C, T> entry) {
        ObjectProperty<C> theCurrentConfiguration = new SimpleObjectProperty<>();

        ConfigurationTreeView<C, T> view = new ConfigurationTreeView<>(
                theCurrentConfiguration,
                runtimeViewProperty,
                true,
                true);
        view.addContextMenuAction(
                "gmi-keyboard-tab:18:#2ecc71",
                Bindings.createStringBinding(() -> "Find deadlock"),
                this::deadlockFinder);

        if (entry.getParentConfiguration() != null) {
            //we set the source configuration first, so that the configuration view computes the differences
            theCurrentConfiguration.set(entry.getParentConfiguration());
        }
        theCurrentConfiguration.set(entry.getConfiguration());

        return view;
    }

    void deadlockFinder(Event event) {
        PreinitializedRuntime<C, T> preinitializedRuntime = new PreinitializedRuntime<>(transitionRelationProperty.get(), Collections.singleton(theCurrentConfiguration.get()));
        IStateSpaceManager stateSpaceManager = new SimpleStateSpaceManager<>();
        stateSpaceManager.parentTransitionStorage();
        IExecutionController deadlockVerifier = DeadlockVerifier.bfs(preinitializedRuntime, stateSpaceManager);
        deadlockVerifier.getAnnouncer().when(FinalStateDetected.class, (ann, e) -> deadlockFoundCallback(e, stateSpaceManager, this::addCounterExample));
        deadlockVerifier.getAnnouncer().when(PropertyEvent.class, (ann, e) ->  {
            if (e.isVerified()) {
                Alert alert =  new Alert(Alert.AlertType.INFORMATION, "No deadlock found");
                alert.setTitle("Congratulations");
                alert.setHeaderText(null);
                alert.showAndWait();
            }
        });
        deadlockVerifier.execute();
    }

    void deadlockFoundCallback(FinalStateDetected event, IStateSpaceManager stateSpaceManager, Consumer<List<C>> counterExampleHandler) {
        List<C> counterExample = new DijkstraShortestPath<>()
                .getShortestPath(
                        stateSpaceManager.getGraphView(),
                        stateSpaceManager.initialConfigurations(),
                        event.getFinalState());
        counterExampleHandler.accept(counterExample);
    }

    Set<C> getInitialConfigurations(Set<C> initialSet, ITransitionRelation<C, T> transitionRelation) {
        if (transitionRelation == null) return Collections.emptySet();
        if (initialSet == null) {
            initialSet = transitionRelation.initialConfigurations();
        }
        return initialSet;
    }

    public void addCounterExample(List<C> counterExample) {
        branchingTraceView.setCounterExample(counterExample);
    }
}

package plug.ui2.simulation.ui;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.control.SplitPane;
import javafx.stage.FileChooser;
import plug.core.*;
import plug.core.fx.IAppContext;
import plug.core.registry.LanguageModuleRegistry;
import plug.ui2.model_view.ModelEditor;
import plug.ui2.simulation.SimulationView;
import plug.utils.file.FileWatcher;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimulationMainView<C, T> extends SplitPane {
    ObjectProperty<Path> modelPath = new SimpleObjectProperty<>();
    BooleanProperty modelChanged = new SimpleBooleanProperty(false);
    FileWatcher watcher;

    ObjectProperty<ITransitionRelation<C,T>> transitionRelationProperty = new SimpleObjectProperty<>();
    ObjectProperty<IRuntimeView<C,T>> runtimeViewProperty = new SimpleObjectProperty<>();
    ObjectProperty<Set<C>> initialSetProperty = new SimpleObjectProperty<>();


    public SimulationMainView(IAppContext appContext) {
        watcher = appContext.getFileWatcher();

        modelPath.addListener(this::modelChangedCallback);

        ModelEditor modelUnderVerificationView = new ModelEditor("Model", modelPath, modelChanged, modelExtensionFilters());


        modelChanged.addListener((a, o, n) -> {
            updateSimulationView();
        });

        setOrientation(Orientation.VERTICAL);
        setDividerPosition(0, .5);

        getItems().addAll(modelUnderVerificationView, createSimulationView());
    }

    SimulationView<C, T> createSimulationView() {
        return new SimulationView<>(
                transitionRelationProperty,
                runtimeViewProperty,
                initialSetProperty);
    }

    public void updateSimulationView() {
        if (modelPath.get() == null) {
            transitionRelationProperty.set(null);
            runtimeViewProperty.set(null);
            initialSetProperty.set(null);
            return;
        }
        ILanguagePlugin<ITransitionRelation<C, T>> plugin = LanguageModuleRegistry.getInstance().getModuleByExtension(modelPath.get());

        ITransitionRelation<C, T> transitionRelation;
        try {
            transitionRelation = plugin.getLoader().getRuntime(modelPath.get().toUri(), null);
            transitionRelationProperty.set( transitionRelation );
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (transitionRelation == null) return;

        runtimeViewProperty.set((IRuntimeView<C, T>) plugin.getRuntimeView(transitionRelation));
        initialSetProperty.set(transitionRelation.initialConfigurations());

        getItems().remove(1);
        getItems().add(createSimulationView());
    }

    public void modelChangedCallback(ObservableValue<? extends Path> obj, Path oldP, Path newP) {
        watcher.removePathListener(oldP);
        watcher.addOnContentChange(newP, (c) -> Platform.runLater(() -> {
                modelChanged.set(!modelChanged.get());
        }));
        modelChanged.set(!modelChanged.get());
    }


    public Collection<FileChooser.ExtensionFilter> modelExtensionFilters () {
        List<FileChooser.ExtensionFilter> extensionFilters = new ArrayList<>();
        for (ILanguagePlugin languageModule : LanguageModuleRegistry.getInstance().getModules()) {
            if (languageModule.getName().equals("Buchi")) {continue;}
            List<String> extensions = Stream.of(languageModule.getExtensions()).map(e -> "*" + e).collect(Collectors.toList());
            extensionFilters.add(new FileChooser.ExtensionFilter(languageModule.getName(), extensions));
        }
        return extensionFilters;
    }
}
